{
	"blt_version" : 2,
	"name" : "Crew Ability: Spotter",
	"description" : "Bots mark guards during stealth game.",
	"author" : "TdlQ\n    chinese translation by LR_Daring\n    spanish translation by Kilowide",
	"image" : "tdlq.dds",
	"color" : "0.52 1 0",
	"contact" : "",
	"version" : "8",
	"simple_update_url" : "http://pd2mods.z77.fr/update/CrewAbilitySpotter.zip",
	"simple_dependencies" : {
		"Keepers": "http://pd2mods.z77.fr/update/Keepers.zip"
	},
	"updates": [
		{
			"identifier" : "SimpleModUpdater",
			"display_name" : "Simple Mod Updater",
			"install_folder" : "Simple Mod Updater",
			"host" : { "meta": "http://pd2mods.z77.fr/meta/SimpleModUpdater", }
		}
	],
	"hooks" : [
		{
			"hook_id" : "lib/managers/menu/crewmanagementgui",
			"script_path" : "lua/crewmanagementgui.lua"
		},
		{
			"hook_id" : "lib/network/base/hostnetworksession",
			"script_path" : "lua/hostnetworksession.lua"
		},
		{
			"hook_id" : "lib/units/player_team/teamaibase",
			"script_path" : "lua/teamaibase.lua"
		},
		{
			"hook_id" : "lib/units/player_team/logics/teamailogicidle",
			"script_path" : "lua/teamailogicidle.lua"
		},
		{
			"hook_id" : "lib/units/player_team/logics/teamailogictravel",
			"script_path" : "lua/teamailogictravel.lua"
		}
	]
}
