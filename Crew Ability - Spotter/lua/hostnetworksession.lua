local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cas_original_hostnetworksession_chkdropinpeer = HostNetworkSession.chk_drop_in_peer
function HostNetworkSession:chk_drop_in_peer(dropin_peer)
	if dropin_peer:expecting_dropin() and not CrewAbilitySpotter:peer_has_cas(dropin_peer) then
		CrewAbilitySpotter.filter_ability = true
	end

	local result = cas_original_hostnetworksession_chkdropinpeer(self, dropin_peer)

	CrewAbilitySpotter.filter_ability = false

	return result
end
