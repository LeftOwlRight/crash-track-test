local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function TeamAILogicIdle.cas_upd_sneak_spotting(data, my_data)
	if not managers.groupai:state():whisper_mode() then
		TeamAILogicIdle._upd_sneak_spotting = function() end
	elseif managers.player:has_category_upgrade('team', CrewAbilitySpotter.ability_name) then
		if not my_data.cas_mark_t or my_data.cas_mark_t + 1 < data.t then
			my_data.cas_mark_t = data.t
			for key, attention_info in pairs(data.detected_attention_objects) do
				if attention_info.identified and (attention_info.verified or attention_info.nearly_visible) then
					if attention_info.is_person
						and attention_info.char_tweak and attention_info.char_tweak.silent_priority_shout
						and (not attention_info.char_tweak.priority_shout_max_dis or attention_info.dis < attention_info.char_tweak.priority_shout_max_dis)
						and (attention_info.unit:slot() ~= 21 or not attention_info.unit:movement():cool())
						and attention_info.unit:character_damage() and not attention_info.unit:character_damage():dead()
					then
						TeamAILogicIdle.mark_sneak_char(data, data.unit, attention_info.unit, nil, nil)
					elseif managers.groupai:state()._security_cameras[attention_info.unit:key()] then
						attention_info.unit:contour():add('mark_unit', true)
					end
				end
			end

			if data.unit:base().kpr_awaken and data.unit:base().kpr_is_keeper and not data.unit:movement():chk_action_forbidden('turn') then
				local brain = data.unit:brain()
				if brain:objective() and brain:objective().in_place then
					if not my_data.cas_turn_t or my_data.cas_turn_t + 3 < data.t then
						my_data.cas_turn_t = data.t
						brain:action_request({
							type = 'turn',
							body_part = 2,
							angle = (math.random() - 0.5) * 360
						})
					end
				end
			end
		end
	end
end
