	if RequiredScript == "lib/tweak_data/weaponfactorytweakdata" then
    Hooks:PostHook(WeaponFactoryTweakData, "init", "IWAMInit", function(self)
	
		self.parts.wpn_fps_upg_ass_m4_upper_reciever_ballos.override.wpn_fps_amcar_uupg_body_upperreciever = {
			a_obj = "a_o",
			unit = "units/payday2/weapons/wpn_fps_ass_m16_pts/wpn_fps_ass_m16_o_handle_sight"
		}
		self.parts.wpn_fps_upg_ass_m4_upper_reciever_ballos.override.wpn_fps_m4_upper_reciever_round_vanilla = {
			unit = "units/payday2/weapons/wpn_upg_dummy/wpn_upg_dummy"	
		}
	    self.parts.wpn_fps_upg_ass_m4_upper_reciever_core.override.wpn_fps_amcar_uupg_body_upperreciever = {
			a_obj = "a_o",
			unit = "units/payday2/weapons/wpn_fps_ass_m16_pts/wpn_fps_ass_m16_o_handle_sight"
		}
		self.parts.wpn_fps_upg_ass_m4_upper_reciever_core.override.wpn_fps_m4_upper_reciever_round_vanilla = {
			unit = "units/payday2/weapons/wpn_upg_dummy/wpn_upg_dummy"	
		}
    table.map_append(self.parts.wpn_fps_m4_uupg_fg_lr300, {
		forbids = {
			"wpn_fps_m4_uupg_fg_rail_ext"
		}
	})
	if not self.parts.wpn_fps_m4_upper_reciever_edge.override then
			self.parts.wpn_fps_m4_upper_reciever_edge.override = {}
		end
	self.parts.wpn_fps_m4_upper_reciever_edge.override.wpn_fps_m4_upper_reciever_round_vanilla = {
			unit = "units/payday2/weapons/wpn_upg_dummy/wpn_upg_dummy"
		}
		wpn_fps_amcar_uupg_body_upperreciever = {
			a_obj = "a_o",
			unit = "units/payday2/weapons/wpn_fps_ass_m16_pts/wpn_fps_ass_m16_o_handle_sight"
		}
	local car_override_table = {
		"wpn_fps_ass_amcar",
		"wpn_fps_ass_m16",
		"wpn_fps_ass_tecci",
		"wpn_fps_ass_m4",
		"wpn_fps_smg_olympic",
		"wpn_fps_ass_contraband"
		}
	for index, weapon_id in ipairs(car_override_table) do
		if not self[weapon_id].override then
			self[weapon_id].override = {}
		end
	end
		self.wpn_fps_ass_amcar.override.wpn_fps_upg_m4_m_quad = {                                                             
			stats = { value = 3, spread_moving = -2, extra_ammo = 19, recoil = 1, concealment = -4, spread = -1}
		}			
		self.wpn_fps_ass_amcar.override.wpn_fps_m4_uupg_fg_lr300 = {
			stats = { value = 3, spread_moving = -1, concealment = -1, recoil = -1, spread = 2}
		}			
		self.wpn_fps_ass_amcar.override.wpn_fps_m4_upg_m_quick = {
			stats = { value = 2, concealment = -2, reload = 10, extra_ammo = 4}
		}			
		self.wpn_fps_ass_amcar.override.wpn_fps_upg_ass_m4_upper_reciever_ballos = {
		    adds = {	
			   "wpn_fps_m4_uupg_draghandle"	  
			}
		}			
		self.wpn_fps_ass_amcar.override.wpn_fps_upg_ass_m4_upper_reciever_core = {
		    adds = {		
			   "wpn_fps_m4_uupg_draghandle"
			}
		}			
		self.wpn_fps_ass_amcar.override.wpn_fps_m4_upper_reciever_edge = {
		    adds = {			
			   "wpn_fps_m4_uupg_draghandle"
			}
		}		
		self.wpn_fps_ass_amcar.override.wpn_fps_upg_m4_m_pmag = {
			stats = { value = 3, spread_moving = 1, concealment = -1, recoil = 1, extra_ammo = 6}
		}		
		self.wpn_fps_ass_amcar.wpn_fps_smg_olympic_s_short = {
			stats = { value = 3, spread_moving = -1, concealment = 3, spread = -1}
		}
		self.wpn_fps_ass_amcar.override.wpn_fps_m4_uupg_m_std = {
			stats = { value = 1, extra_ammo = 4, concealment = -1}
		}
		self.wpn_fps_ass_amcar.override.wpn_fps_ass_l85a2_m_emag = {
			stats = { value = 1,recoil = 1, concealment = -1, extra_ammo = 6}
		}
		self.wpn_fps_ass_amcar.override.wpn_fps_upg_m4_m_l5 = {
			stats = { value = 1, extra_ammo = 6, concealment = -1, recoil = 1}
	    }
		self.wpn_fps_ass_m16.override.wpn_fps_upg_m4_m_quad = {
			stats = {value = 3, spread_moving = -2, extra_ammo = 19, recoil = 1, concealment = -4, spread = -1}
		}		
		self.wpn_fps_ass_m16.override.wpn_fps_upg_m4_m_pmag = {
			stats = {value = 3,spread_moving = 1,concealment = -1,recoil = 1,extra_ammo = 6}
		}			
		self.wpn_fps_ass_m16.override.wpn_fps_m4_upg_m_quick = {
			stats = {value = 2,concealment = -2,reload = 10,extra_ammo = 4}
		}			
		self.wpn_fps_ass_m16.override.wpn_fps_upg_fg_smr = {
			stats = {value = 3,spread_moving = -1,recoil = - 2,spread = 2}
		}			
		self.wpn_fps_ass_m16.override.wpn_fps_upg_fg_jp = {
			stats = {value = 3,spread_moving = -1,concealment = 2,recoil = -1,spread = 1}
		}
		self.wpn_fps_ass_m16.override.wpn_fps_m4_uupg_m_std = {
			stats = {value = 1,extra_ammo = 4,concealment = -1}
		}			
		self.wpn_fps_ass_m16.override.wpn_fps_smg_olympic_s_short = {
			stats = {value = 3,spread_moving = -1,concealment = 3,spread = -1}
		}
		self.wpn_fps_ass_m16.override.wpn_fps_ass_l85a2_m_emag = {
			stats = {value = 1,recoil = 1,concealment = -1,extra_ammo = 6}
		}
		self.wpn_fps_ass_m16.override.wpn_fps_upg_m4_m_l5 = {
			stats = {value = 1,extra_ammo = 6,recoil = 1,concealment = -1}
		}
	    self.wpn_fps_ass_tecci.override.wpn_fps_smg_olympic_s_short = {
			stats = {value = 3,spread_moving = -1,concealment = 3,spread = -1}
		}
	    self.wpn_fps_ass_m4.override.wpn_fps_smg_olympic_s_short = {
			stats = {value = 3,spread_moving = -1,concealment = 3,spread = -1}
	    }	
		self.wpn_fps_ass_m4.override.wpn_fps_m16_fg_railed = {
			stats = {value = 3,concealment = -1,recoil = -2,spread = 2}
		}
	    self.wpn_fps_smg_olympic.override.wpn_fps_upg_m4_m_quad = {
			stats = {value = 3,spread_moving = -2,extra_ammo = 19,recoil = 1,concealment = -4,spread = -1}
		}			
		self.wpn_fps_smg_olympic.wpn_fps_m4_upg_m_quick = {
			stats = {value = 2,concealment = -2,reload = 10,extra_ammo = 4}
		}		
		self.wpn_fps_smg_olympic.wpn_fps_upg_m4_m_pmag = {
			stats = {value = 3,spread_moving = 1,recoil = 1,concealment = -1,extra_ammo = 6}
		}
		self.wpn_fps_smg_olympic.wpn_fps_m4_uupg_m_std = {
			stats = {value = 1,extra_ammo = 4,concealment = -1}
		}	
		self.wpn_fps_smg_olympic.wpn_fps_m4_uupg_s_fold = {
			stats = {value = 3,spread_moving = -1,concealment = 2,spread = -1}
		}
		self.wpn_fps_smg_olympic.wpn_fps_ass_l85a2_m_emag = {
			stats = {value = 1,recoil = 1,concealment = -1,extra_ammo = 6}
		}
		self.wpn_fps_smg_olympic.wpn_fps_upg_m4_m_l5 = {
			stats = {value = 1,recoil = 1,extra_ammo = 6,concealment = -1,}
		}
        self.wpn_fps_ass_contraband.wpn_fps_smg_olympic_s_short = {
			stats = { value = 3, spread_moving = -1, concealment = 3, spread = -1}
		}
	local sight_adds_table = {
		"wpn_fps_snp_winchester",
		"wpn_fps_sho_boot",
		"wpn_fps_pis_c96",
		"wpn_fps_shot_m37",
		"wpn_fps_smg_baka",
		"wpn_fps_gre_china",
		"wpn_fps_bow_arblast",
		"wpn_fps_bow_frankish",
		"wpn_fps_bow_hunter"
		}
	for index, weapon_id in ipairs(sight_adds_table) do
		if not self[weapon_id].adds then
			self[weapon_id].adds = {}
		end
	end
    local sight_override_table = {
		"wpn_fps_snp_winchester",
		"wpn_fps_sho_boot",
		"wpn_fps_pis_c96",
		"wpn_fps_shot_m37",
		"wpn_fps_smg_baka",
		"wpn_fps_gre_china",
		"wpn_fps_bow_arblast",
		"wpn_fps_bow_frankish",
		"wpn_fps_bow_hunter"
		}
	for index, weapon_id in ipairs(sight_override_table) do
		if not self[weapon_id].override then
			self[weapon_id].override = {}
		end
	end
	--m37
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 3, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}	
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}	
	}
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 1, -3.7),rotation = Rotation(0, -0.499, -0)}}	
    }
	self.wpn_fps_shot_m37.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_shot_m37 = {translation = Vector3(0, 6, -3.7),rotation = Rotation(0, -0.499, -0)}}
	}
	
	self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_smg_thompson_o_adapter"
    }
    self.wpn_fps_shot_m37.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_smg_thompson_o_adapter"		    
    }
    --arblast
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 9, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}	
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}	
	}
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 5, 2),rotation = Rotation(-0, -5, 0)}}	
    }
	self.wpn_fps_bow_arblast.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_bow_arblast = {translation = Vector3(0, 10, 2),rotation = Rotation(-0, -5, 0)}}	
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_smg_baka_o_adapter"
    }
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_smg_baka_o_adapter"		    
    }
	self.wpn_fps_bow_arblast.adds.wpn_fps_upg_fl_pis_m3x = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_fl_pis_x400v = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_fl_pis_crimson = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_2006m_fl_adapter"
    }
    self.wpn_fps_bow_arblast.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_2006m_fl_adapter"		    
    }
	--hunter
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}	
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}	
	}
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 16, 0.5)}}	
    }
	self.wpn_fps_bow_hunter.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_bow_hunter = {translation = Vector3(0, 23, 0.5)}}	
	}
    
	self.wpn_fps_bow_hunter.adds.wpn_fps_upg_fl_pis_m3x = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_hunter.adds.wpn_fps_upg_fl_pis_x400v = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_hunter.adds.wpn_fps_upg_fl_pis_crimson = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_hunter.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_2006m_fl_adapter"
    }
    self.wpn_fps_bow_hunter.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_2006m_fl_adapter"		    
    }
    --winchester
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -11, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}	
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}	
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}	
    }
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -9, -3.2),rotation = Rotation(0, -0.086, -0)}}	
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -16, -3.2),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_leupold = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -38, -4.12),rotation = Rotation(0, -0.086, -0)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_45iron = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(-5.6, -7, -14.4),rotation = Rotation(0, -0.086, -45)}}
	}
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_45rds = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(-5.45, -7, -14.95),rotation = Rotation(0, -0.086, -45)}}
    }
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_45rds_v2 = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(-5.45, -7, -14.95),rotation = Rotation(0, -0.086, -45)}}
	}	
	self.wpn_fps_snp_winchester.override.wpn_fps_upg_o_box = {
		stance_mod = {wpn_fps_snp_winchester = {translation = Vector3(0, -32, -4.12),rotation = Rotation(0, -0.086, -0)}}
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_smg_thompson_o_adapter"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_smg_thompson_o_adapter"
    }
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_smg_thompson_o_adapter"		    
    }
	self.wpn_fps_snp_winchester.adds.wpn_fps_upg_fl_pis_m3x = {
		"wpn_fps_pis_sparrow_fl_dummy"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_fl_pis_x400v = {
		"wpn_fps_pis_sparrow_fl_dummy"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_fl_pis_crimson = {
		"wpn_fps_pis_sparrow_fl_dummy"
	}
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_sparrow_fl_dummy"
    }
    self.wpn_fps_snp_winchester.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_sparrow_fl_dummy"		    
    }
    --frankish
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 6, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}	
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}	
	}
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 5, 2.4),rotation = Rotation(-0, -5, 0)}}	
    }
	self.wpn_fps_bow_frankish.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_bow_frankish = {translation = Vector3(0, 10, 2.4),rotation = Rotation(-0, -5, 0)}}
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_smg_baka_o_adapter"
    }
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_smg_baka_o_adapter"		    
    }
	self.wpn_fps_bow_frankish.adds.wpn_fps_upg_fl_pis_m3x = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_fl_pis_x400v = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_fl_pis_crimson = {
		"wpn_fps_pis_2006m_fl_adapter"
	}
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_2006m_fl_adapter"
    }
    self.wpn_fps_bow_frankish.adds.wpn_fps_upg_fl_pis_tlr1 = {
		"wpn_fps_pis_2006m_fl_adapter"		    
    }
    --boot
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, 2, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}	
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}	
	}
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, -1, -4.2)}}	
    }
	self.wpn_fps_sho_boot.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_sho_boot = {translation = Vector3(0, 4, -4.2)}}	
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_rpg7_sight_adapter"
	}
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_rpg7_sight_adapter"
    }
    self.wpn_fps_sho_boot.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_rpg7_sight_adapter"		    
    }
	--baka
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,1,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}	
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}	
	}
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,0,-6.06)}}	
    }
	self.wpn_fps_smg_baka.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_smg_baka = {translation = Vector3(0.87,2,-6.06)}}	
	}
	self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_smg_baka_o_adapter"
	}
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_smg_baka_o_adapter"
    }
    self.wpn_fps_smg_baka.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_smg_baka_o_adapter"		    
    }
	--china
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_specter = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_aimpoint = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_aimpoint_2 = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_docter = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_eotech = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_t1micro = {
		forbids = {
	        "wpn_fps_gre_m79_sight_up",
	        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_cmore = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_acog = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
 	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -3, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_cs = {
		forbids = {
	        "wpn_fps_gre_m79_sight_up",
	        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_eotech_xps = {
		forbids = {
	        "wpn_fps_gre_m79_sight_up",
	        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_reflex = {
    	forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_rx01 = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}	
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_rx30 = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}	
	}
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_spot = {
		forbids = {
		        "wpn_fps_gre_m79_sight_up",
		        "wpn_fps_gre_m79_sight_down"
	    },
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -8, 0),rotation = Rotation(0, -1.999, 0)}}	
    }
	self.wpn_fps_gre_china.override.wpn_fps_upg_o_xpsg33_magnifier = {
		forbids = {
			"wpn_fps_gre_m79_sight_up",
			"wpn_fps_gre_m79_sight_down"
		},
		stance_mod = {wpn_fps_gre_china = {translation = Vector3(0, -2, 0),rotation = Rotation(0, -1.999, 0)}}
	}
	
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_snp_mosin_rail"
	}
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_snp_mosin_rail"
    }
    self.wpn_fps_gre_china.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_snp_mosin_rail"		    
    }
    --c96
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_specter = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_aimpoint = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_aimpoint_2 = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_docter = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_eotech = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_t1micro = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_cmore = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_acog = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -5, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_cs = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_eotech_xps = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_reflex = {
    	stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_rx01 = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}	
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_rx30 = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}	
	}
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_spot = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -7, 0.1)}}	
    }
	self.wpn_fps_pis_c96.override.wpn_fps_upg_o_xpsg33_magnifier = {
		stance_mod = {wpn_fps_pis_c96 = {translation = Vector3(-3.39, -5, 0.1)}}	
	}
	self.wpn_fps_pis_c96.override.wpn_fps_pis_c96_sight = {
        stats = {value = 1,zoom = 7,recoil = 1,concealment = -3}
	}
	self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_specter = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_aimpoint = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_aimpoint_2 = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_docter = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_eotech = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_t1micro = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_cmore = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_acog = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_cs = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_eotech_xps = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_reflex = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_rx01 = {
		"wpn_fps_pis_c96_rail"
	}
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_rx30 = {
		"wpn_fps_pis_c96_rail"
    }
    self.wpn_fps_pis_c96.adds.wpn_fps_upg_o_spot = {
		"wpn_fps_pis_c96_rail"
    }
    if not self.wpn_fps_ass_g36.override then
			self.wpn_fps_ass_g36.override = {}
		end
	self.wpn_fps_ass_g36.override.wpn_fps_upg_m4_s_ubr = {
		    adds = {
		       "wpn_fps_ass_g36_body_standard",		
			   "wpn_fps_ass_g36_g_standard"
		    }
		}
		self.wpn_fps_ass_g36.override.wpn_fps_upg_m4_s_mk46 = {
		    adds = {
		       "wpn_fps_ass_g36_body_standard",		
			   "wpn_fps_ass_g36_g_standard"
		    }
		}
		self.wpn_fps_ass_g36.override.wpn_fps_upg_m4_s_crane = {
		    adds = {
		       "wpn_fps_ass_g36_body_standard",		
			   "wpn_fps_ass_g36_g_standard"
		    }
		}
		self.wpn_fps_ass_g36.override.wpn_fps_upg_m4_s_standard = {
		    adds = {
		       "wpn_fps_ass_g36_body_standard",		
			   "wpn_fps_ass_g36_g_standard"
		    }
		}
		self.wpn_fps_ass_g36.override.wpn_fps_upg_m4_s_pts = {
		    adds = {
		       "wpn_fps_ass_g36_body_standard",		
			   "wpn_fps_ass_g36_g_standard"
		    }
		}
		self.wpn_fps_ass_g36.override.wpn_fps_snp_tti_s_vltor = {
		    adds = {
		       "wpn_fps_ass_g36_body_standard",		
			   "wpn_fps_ass_g36_g_standard"
		    }
		}
	self.parts.wpn_fps_ass_asval_b_standard.forbids = {
			"wpn_fps_upg_ns_ass_smg_firepig",
            "wpn_fps_upg_ns_ass_smg_stubby",
            "wpn_fps_upg_ns_ass_smg_tank",
            "wpn_fps_upg_ass_ns_battle",
            "wpn_fps_upg_ass_ns_surefire",
            "wpn_fps_upg_ass_ns_linear",
            "wpn_fps_upg_ass_ns_jprifles",
            "wpn_fps_upg_ns_ass_smg_small",
            "wpn_fps_upg_ns_ass_smg_medium",
            "wpn_fps_upg_ns_ass_smg_large"
	}
	self.parts.wpn_fps_lmg_hk21_g_ergo.stats = {value = 2, recoil = 1, spread = 1, concealment = -1}
	self.parts.wpn_fps_upg_ass_m4_lower_reciever_core.stats = {value = 4, recoil = -2, damage = 5}
	self.parts.wpn_fps_upg_ass_m4_upper_reciever_core.stats = {value = 4, spread = -2, damage = 7}
	self.parts.wpn_fps_upg_ns_ass_pbs1.stats = {value = 2, suppression = 12, alert_size = 12, spread = 1, recoil = 2, concealment = -3}
	self.parts.wpn_upg_ak_s_psl.stats = {value = 4, spread = 3,recoil = 1, spread_moving = -3, concealment = -5}
	self.parts.wpn_fps_upg_ak_s_solidstock.stats = {value = 3,recoil = 3, spread_moving = -2, concealment = -2}
	self.parts.wpn_fps_upg_fl_pis_laser.stats = {value = 2, spread_moving = -1}
	self.parts.wpn_fps_lmg_rpk_fg_standard.stats = {value = 2, spread = 1, recoil = 1, concealment = 1}
	self.parts.wpn_fps_lmg_rpk_s_standard.stats = {value = 3, spread = 2, concealment = -4}
	self.parts.wpn_fps_smg_olympic_fg_railed.stats = {value = 3, spread_moving = -1, recoil = 1, concealment = 1}
	self.parts.wpn_fps_m4_uupg_fg_lr300.stats = {value = 3, spread_moving = 1, recoil = 3, concealment = 2}
	self.parts.wpn_fps_upg_m4_g_sniper.stats = {value = 4, spread_moving = -2, spread = 2, concealment = -2}
	self.parts.wpn_fps_upg_m4_s_pts.stats = {value = 3, recoil = -1, spread_moving = 1, spread = 1, concealment = 1}
	self.parts.wpn_fps_upg_m4_g_ergo.stats = {value = 2, recoil = 1, spread_moving = 2, concealment = 1}
	self.parts.wpn_fps_snp_tti_g_grippy.stats = {value = 2, recoil = 2, concealment = -1}
	self.parts.wpn_fps_snp_tti_s_vltor.stats = {value = 2, recoil = 2, concealment = -1}
	self.parts.wpn_fps_upg_m4_s_standard.stats = {value = 2, recoil = 1}
	self.parts.wpn_fps_ass_asval_s_solid.stats = {value = 2, recoil = 3, spread_moving = -2, concealment = -4, spread = 1}
	self.parts.wpn_fps_ass_asval_b_proto.stats = {value =4, damage = -2, recoil = -2, concealment = 2, spread = -2}
	self.parts.wpn_fps_ass_asval_b_proto.perks = nil	
	self.parts.wpn_fps_ass_asval_b_proto.sub_type = nil	
	self.parts.wpn_fps_upg_m4_s_pts.forbids = nil
	self.parts.wpn_fps_upg_m4_s_mk46.forbids = nil
	self.parts.wpn_fps_upg_m4_s_crane.forbids = nil
	self.parts.wpn_fps_upg_m4_s_ubr.forbids = nil
	self.parts.wpn_fps_upg_m4_s_standard.forbids = nil
	self.parts.wpn_fps_snp_tti_s_vltor.forbids = nil	
	self.parts.wpn_fps_shot_r870_s_m4.forbids = {
	        "wpn_fps_shot_r870_ris_special"
	}
	self.wpn_fps_ass_asval.adds.wpn_fps_ass_asval_body_standard = {
		"wpn_fps_ass_asval_g_standard"
	}
	self.parts.wpn_upg_ak_s_adapter.adds = nil
	self.parts.wpn_fps_upg_ak_s_solidstock.adds = nil    
	self.parts.wpn_upg_ak_s_folding.adds = nil
	self.wpn_fps_smg_x_mp5.override.wpn_fps_smg_schakal_vg_surefire = {
		stats = {value =1, recoil = 2, concealment = -3}
	}
	self.wpn_fps_smg_x_akmsu.override.wpn_fps_smg_schakal_vg_surefire = {
		stats = {value =1, recoil = 2, concealment = -3}
	}
	table.map_append(self.parts.wpn_fps_ass_ak_body_lowerreceiver_gold, {
		forbids = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
		}
	})
	table.map_append(self.parts.wpn_fps_ass_ak_body_lowerreceiver, {
		forbids = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
			}
	})
	table.map_append(self.parts.wpn_fps_smg_akmsu_body_lowerreceiver, {
		forbids = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
			}
	})
	table.map_append(self.parts.wpn_fps_ass_galil_body_standard, {
		adds = {
			"wpn_fps_ass_galil_g_standard"
		}
	})
	table.map_append(self.parts.wpn_fps_lmg_rpk_fg_standard, {
		adds = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
		}
	})
	table.map_append(self.parts.wpn_upg_ak_fg_combo1, {
		adds = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
		}
	})
	table.map_append(self.parts.wpn_upg_ak_fg_combo3, {
		adds = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
		}
	})
	table.map_append(self.parts.wpn_upg_ak_fg_combo4, {
		adds = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
		}
	})
	table.map_append(self.parts.wpn_fps_upg_ak_fg_trax, {
		adds = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip"
		}
	})
	self.parts.wpn_fps_upg_ak_fg_tapco.adds = {
		"wpn_fps_upg_vg_ass_smg_verticalgrip",
	    "wpn_fps_addon_ris"
	}
	table.map_append(self.parts.wpn_fps_smg_mp5_fg_mp5a4, {
		forbids = {
			"wpn_fps_smg_schakal_vg_surefire"
		}
	})
	table.map_append(self.parts.wpn_fps_smg_mp5_fg_flash, {
		forbids = {
			"wpn_fps_smg_schakal_vg_surefire"
		}
	})
	table.map_append(self.parts.wpn_fps_smg_mp5_fg_mp5sd, {
		forbids = {
			"wpn_fps_smg_schakal_vg_surefire"
		}
	})	
	table.map_append(self.parts.wpn_fps_ass_galil_g_sniper, {
		forbids = {
			"wpn_fps_ass_galil_g_standard"
		}
	})	
	table.map_append(self.parts.wpn_fps_smg_akmsu_fg_standard, {
		forbids = {
			"wpn_fps_smg_schakal_vg_surefire"
		}
	})		
	table.map_append(self.parts.wpn_fps_lmg_rpk_fg_wood, {
		forbids = {
			"wpn_fps_smg_schakal_vg_surefire"
		}
	})	 
    table.map_append(self.parts.wpn_fps_smg_schakal_vg_surefire, {
		forbids = {
			"wpn_fps_upg_vg_ass_smg_verticalgrip",
		    "wpn_fps_upg_vg_ass_smg_stubby"
		}
	})	 
	 table.map_append(self.parts.wpn_fps_smg_uzi_fg_standard, {
		forbids = {
			"wpn_fps_smg_schakal_vg_surefire"
		}
	})	 
	self.wpn_fps_smg_x_mp5.default_blueprint = {
		"wpn_fps_smg_mp5_body_mp5",
		"wpn_fps_smg_mp5_fg_mp5a4",
		"wpn_fps_smg_mp5_m_std",
		"wpn_fps_smg_mp5_s_solid"
	}
table.insert(self.parts.wpn_upg_ak_s_psl.forbids, "wpn_fps_ass_galil_g_sniper")
table.insert(self.parts.wpn_upg_ak_s_psl.forbids, "wpn_fps_ass_galil_g_standard")	
--M320
table.insert(self.wpn_fps_gre_slap.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--Luger
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_medium")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_medium_gem")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_large_kac")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_large")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_medium_slim")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_ass_filter")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_jungle")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_pis_ns_flash")
table.insert(self.wpn_fps_pis_breech.uses_parts, "wpn_fps_upg_ns_pis_small")
--G3
table.insert(self.wpn_fps_ass_g3.uses_parts, "wpn_fps_smg_mp5_s_adjust")	 
table.insert(self.wpn_fps_ass_g3.uses_parts, "wpn_fps_lmg_hk21_g_ergo")
--HK21
table.insert(self.wpn_fps_lmg_hk21.uses_parts, "wpn_fps_ass_g3_g_sniper")
table.insert(self.wpn_fps_lmg_hk21.uses_parts, "wpn_fps_ass_g3_g_retro")
--SAW
table.insert(self.wpn_fps_saw.uses_parts, "wpn_fps_upg_fl_ass_smg_sho_peqbox")
table.insert(self.wpn_fps_saw.uses_parts, "wpn_fps_upg_fl_ass_smg_sho_surefire")
table.insert(self.wpn_fps_saw.uses_parts, "wpn_fps_upg_fl_ass_peq15")	
table.insert(self.wpn_fps_saw.uses_parts, "wpn_fps_upg_fl_ass_laser")
table.insert(self.wpn_fps_saw.uses_parts, "wpn_fps_upg_fl_ass_utg")           
table.insert(self.wpn_fps_saw_secondary.uses_parts, "wpn_fps_upg_fl_ass_smg_sho_peqbox")
table.insert(self.wpn_fps_saw_secondary.uses_parts, "wpn_fps_upg_fl_ass_smg_sho_surefire")
table.insert(self.wpn_fps_saw_secondary.uses_parts, "wpn_fps_upg_fl_ass_peq15")	
table.insert(self.wpn_fps_saw_secondary.uses_parts, "wpn_fps_upg_fl_ass_laser")
table.insert(self.wpn_fps_saw_secondary.uses_parts, "wpn_fps_upg_fl_ass_utg")   
--FlameThrower
table.insert(self.wpn_fps_fla_mk2.uses_parts, "wpn_fps_upg_fl_ass_smg_sho_peqbox")
table.insert(self.wpn_fps_fla_mk2.uses_parts, "wpn_fps_upg_fl_ass_smg_sho_surefire")
table.insert(self.wpn_fps_fla_mk2.uses_parts, "wpn_fps_upg_fl_ass_peq15")	
table.insert(self.wpn_fps_fla_mk2.uses_parts, "wpn_fps_upg_fl_ass_laser")
table.insert(self.wpn_fps_fla_mk2.uses_parts, "wpn_fps_upg_fl_ass_utg")           
--AK74
table.insert(self.wpn_fps_ass_74.uses_parts, "wpn_fps_lmg_rpk_s_standard")
table.insert(self.wpn_fps_ass_74.uses_parts, "wpn_fps_lmg_rpk_fg_standard")
table.insert(self.wpn_fps_ass_74.default_blueprint, "wpn_upg_ak_g_standard")
--AKM
table.insert(self.wpn_fps_ass_akm_gold.uses_parts, "wpn_fps_lmg_rpk_s_standard")
table.insert(self.wpn_fps_ass_akm_gold.uses_parts, "wpn_fps_lmg_rpk_fg_standard")
table.insert(self.wpn_fps_ass_akm.uses_parts, "wpn_fps_lmg_rpk_s_standard")
table.insert(self.wpn_fps_ass_akm.uses_parts, "wpn_fps_lmg_rpk_fg_standard")
--Akimbo AMSU
self.wpn_fps_smg_x_akmsu.stock_adapter = "wpn_upg_ak_s_adapter"
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_upg_m4_s_pts")	   
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_snp_tti_s_vltor")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_upg_ak_s_solidstock")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_upg_ak_s_skfoldable")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_lmg_rpk_s_standard")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_upg_ak_s_folding_vanilla")
table.insert(self.wpn_fps_smg_x_akmsu.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
table.insert(self.wpn_fps_smg_x_akmsu.default_blueprint, "wpn_upg_ak_s_folding_vanilla")
--AKMSU
table.insert(self.wpn_fps_smg_akmsu.uses_parts, "wpn_fps_lmg_rpk_s_standard")
--HK417
self.wpn_fps_ass_contraband.stock_adapter = "wpn_fps_upg_m4_s_adapter"
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_ass_tecci_ns_special")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_g_mgrip")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_m4_uupg_s_fold")           
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_g_ergo")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_g_sniper")   
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_upg_m4_g_hgrip")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_snp_tti_s_vltor")     
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_snp_tti_g_grippy")
table.insert(self.wpn_fps_ass_contraband.uses_parts, "wpn_fps_smg_olympic_s_short")
--Bizon
self.wpn_fps_smg_coal.stock_adapter = "wpn_fps_smg_polymer_s_adapter"
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_snp_tti_s_vltor")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_upg_ak_s_solidstock")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_upg_ak_g_rk3")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_upg_ak_s_folding")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_upg_m4_s_pts")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_coal.uses_parts, "wpn_fps_lmg_rpk_s_standard")
--KSP58
self.wpn_fps_lmg_par.stock_adapter = "wpn_fps_lmg_m249_s_modern"	
table.insert(self.wpn_fps_lmg_par.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_lmg_par.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_lmg_par.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_lmg_par.uses_parts, "wpn_fps_upg_m4_s_standard")   
table.insert(self.wpn_fps_lmg_par.uses_parts, "wpn_fps_upg_m4_s_pts")   
table.insert(self.wpn_fps_lmg_par.uses_parts, "wpn_fps_snp_tti_s_vltor")
--UMP
self.wpn_fps_smg_schakal.stock_adapter = "wpn_upg_ak_s_adapter"
table.insert(self.wpn_fps_smg_schakal.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_schakal.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_schakal.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_schakal.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_smg_schakal.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_smg_schakal.uses_parts, "wpn_fps_snp_tti_s_vltor")
--G36
self.wpn_fps_ass_g36.stock_adapter = "wpn_upg_ak_s_adapter" 
table.insert(self.wpn_fps_ass_g36.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_g36.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_g36.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_g36.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_g36.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_ass_g36.uses_parts, "wpn_fps_snp_tti_s_vltor")
--skar
self.wpn_fps_ass_scar.stock_adapter = "wpn_fps_ass_s552_s_m4"
table.insert(self.wpn_fps_ass_scar.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_scar.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_scar.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_scar.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_scar.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_ass_scar.uses_parts, "wpn_fps_snp_tti_s_vltor")
--Akimbo MP5
self.wpn_fps_smg_x_mp5.stock_adapter = "wpn_fps_upg_m4_s_adapter"
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_upg_m4_s_pts")	 
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_snp_tti_s_vltor")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_smg_schakal_vg_surefire")	 
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_smg_mp5_s_solid")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_smg_mp5_s_adjust")	 
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_smg_mp5_s_ring")
table.insert(self.wpn_fps_smg_x_mp5.uses_parts, "wpn_fps_smg_mp5_s_folding")
--MP5
self.wpn_fps_smg_mp5.stock_adapter = "wpn_fps_upg_m4_s_adapter"
table.insert(self.wpn_fps_smg_mp5.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_mp5.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_mp5.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_mp5.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_smg_mp5.uses_parts, "wpn_fps_upg_m4_s_pts")	 
table.insert(self.wpn_fps_smg_mp5.uses_parts, "wpn_fps_snp_tti_s_vltor")
--AK5
self.wpn_fps_ass_ak5.stock_adapter = "wpn_fps_ass_s552_s_m4"
table.insert(self.wpn_fps_ass_ak5.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_ak5.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_ak5.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_ak5.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_ak5.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_ass_ak5.uses_parts, "wpn_fps_snp_tti_s_vltor")
--MAC10
self.wpn_fps_smg_mac10.stock_adapter = "wpn_upg_ak_s_adapter"    
table.insert(self.wpn_fps_smg_mac10.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_mac10.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_mac10.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_mac10.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_smg_mac10.uses_parts, "wpn_fps_upg_m4_s_pts")	 
table.insert(self.wpn_fps_smg_mac10.uses_parts, "wpn_fps_snp_tti_s_vltor")
--Fal
self.wpn_fps_ass_fal.stock_adapter = "wpn_upg_ak_s_adapter"
table.insert(self.wpn_fps_ass_fal.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_fal.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_fal.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_fal.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_fal.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_ass_fal.uses_parts, "wpn_fps_snp_tti_s_vltor")
--Galil
self.wpn_fps_ass_galil.stock_adapter = "wpn_upg_ak_s_adapter"
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_upg_ak_s_psl")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_upg_m4_s_pts")	   
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_snp_tti_s_vltor")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_upg_ak_s_solidstock")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_upg_ak_s_folding")
table.insert(self.wpn_fps_ass_galil.uses_parts, "wpn_fps_lmg_rpk_s_standard")
--AsVal
self.wpn_fps_ass_asval.stock_adapter = "wpn_upg_ak_s_adapter" 
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ns_ass_smg_firepig")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ns_ass_smg_stubby")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ns_ass_smg_tank")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ass_ns_battle")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ass_ns_surefire")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ass_ns_linear")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ass_ns_jprifles")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ns_ass_smg_small")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ns_ass_smg_medium")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ns_ass_smg_large")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_snp_tti_s_vltor")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_upg_ak_s_solidstock")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_upg_ak_s_folding")
table.insert(self.wpn_fps_ass_asval.uses_parts, "wpn_fps_lmg_rpk_s_standard")
--Aug
table.insert(self.wpn_fps_ass_aug.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--MP7
table.insert(self.wpn_fps_smg_mp7.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--Hajk
self.wpn_fps_smg_hajk.stock_adapter = "wpn_fps_ass_s552_s_m4"	
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_upg_m4_s_mk46")
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_upg_m4_s_ubr")
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_upg_m4_s_crane")
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_upg_m4_s_standard")
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_upg_m4_s_pts")	
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
table.insert(self.wpn_fps_smg_hajk.uses_parts, "wpn_fps_snp_tti_s_vltor")
--Rota
table.insert(self.wpn_fps_sho_rota.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--Saiga
table.insert(self.wpn_fps_shot_saiga.uses_parts, "wpn_fps_lmg_rpk_s_standard")
--RPK
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_upg_ak_s_psl")
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_upg_ak_fg_combo1")  
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_upg_ak_fg_combo3")
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_upg_ak_fg_combo4")
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_fps_upg_ak_fg_krebs") 
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_fps_upg_ak_fg_trax")
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_fps_upg_ak_fg_tapco")
table.insert(self.wpn_fps_lmg_rpk.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--F2000
table.insert(self.wpn_fps_ass_corgi.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--Uzi
table.insert(self.wpn_fps_smg_uzi.uses_parts, "wpn_fps_smg_schakal_vg_surefire")
--c96
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_pis_c96.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--China
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_gre_china.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--Micro Uzi
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_smg_baka.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--Boot
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_sho_boot.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--w1894
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_fl_pis_laser")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_fl_pis_tlr1")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_fl_pis_x400v")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_fl_pis_crimson")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_fl_pis_m3x")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_45rds_v2")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_45rds")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_box")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_45iron")
table.insert(self.wpn_fps_snp_winchester.uses_parts, "wpn_fps_upg_o_leupold")
--Light Bow
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_fl_pis_laser")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_fl_pis_tlr1")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_fl_pis_x400v")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_fl_pis_crimson")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_fl_pis_m3x")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_bow_frankish.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--Pistol Bow
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_fl_pis_laser")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_fl_pis_tlr1")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_fl_pis_x400v")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_fl_pis_crimson")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_fl_pis_m3x")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_bow_hunter.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--Heavy Bow
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_fl_pis_laser")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_fl_pis_tlr1")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_fl_pis_x400v")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_fl_pis_crimson")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_fl_pis_m3x")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_bow_arblast.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--M37
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_aimpoint")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_cs")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_aimpoint_2")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_docter")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_eotech")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_t1micro")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_cmore")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_acog")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_specter")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_eotech_xps")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_reflex")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_rx01")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_rx30")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_spot")
table.insert(self.wpn_fps_shot_m37.uses_parts, "wpn_fps_upg_o_xpsg33_magnifier")
--M4
table.insert(self.wpn_fps_ass_m4.uses_parts, "wpn_fps_m16_fg_railed")
table.insert(self.wpn_fps_ass_m4.uses_parts, "wpn_fps_smg_olympic_s_short")
--Tecci
table.insert(self.wpn_fps_ass_tecci.uses_parts, "wpn_fps_smg_olympic_s_short")
--M16
table.insert(self.wpn_fps_ass_m16.uses_parts, "wpn_fps_upg_fg_jp")
table.insert(self.wpn_fps_ass_m16.uses_parts, "wpn_fps_m4_uupg_s_fold")
table.insert(self.wpn_fps_ass_m16.uses_parts, "wpn_fps_smg_olympic_s_short")
table.insert(self.wpn_fps_ass_m16.uses_parts, "wpn_fps_upg_fg_smr")
table.insert(self.wpn_fps_ass_m16.uses_parts, "wpn_fps_upg_m4_s_pts")
--para
table.insert(self.wpn_fps_smg_olympic.uses_parts, "wpn_fps_m4_uupg_s_fold")
--AMCAR
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_smg_olympic_s_short")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_m4_uupg_s_fold")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_m4_s_pts")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_m4_uupg_fg_lr300")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_ass_m4_upper_reciever_core")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_ass_m4_upper_reciever_ballos")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_m4_upper_reciever_edge")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_m4_m_pmag")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_m4_g_ergo")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_m4_g_sniper")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_m4_g_mgrip")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_m4_g_hgrip")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_upg_ass_m4_lower_reciever_core")
table.insert(self.wpn_fps_ass_amcar.uses_parts, "wpn_fps_ass_l85a2_m_emag")
	end)
end	