local text_original = LocalizationManager.text
local testAllStrings = false
function LocalizationManager:text(string_id, ...)
return string_id == "bm_menu_vertical_grip_plural" and "vertical grips"                    
or string_id == "bm_wp_upg_s_none"    and "No Stock"                      

or testAllStrings == true and string_id
or text_original(self, string_id, ...)	
end