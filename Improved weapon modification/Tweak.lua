local old_init = WeaponTweakData.init

function WeaponTweakData:init(tweak_data)
    old_init(self, tweak_data)
	
self.amcar.CLIP_AMMO_MAX = 22
self.olympic.CLIP_AMMO_MAX = 22
self.m16.CLIP_AMMO_MAX = 22
end