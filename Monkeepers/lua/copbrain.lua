local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local mkp_original_copbrain_isavailableforassignment = CopBrain.is_available_for_assignment
function CopBrain:is_available_for_assignment(objective)
	local old_objective = self._logic_data.objective
	if old_objective and old_objective.in_place and old_objective.mkp_route and old_objective.kpr_icon == 'pd2_loot' then
		-- don't cancel a drop objective completed at 99%
		return
	end

	return mkp_original_copbrain_isavailableforassignment(self, objective)
end
