local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cap_original_teamaibrain_onlongdisinteracted = TeamAIBrain.on_long_dis_interacted
function TeamAIBrain:on_long_dis_interacted(amount, other_unit, secondary)
	if secondary then
		-- qued
	elseif alive(other_unit) and other_unit:in_slot(2, 3, 4, 5) then
		if CrewAbilityPockets.dis(self._unit:position(), other_unit:position()) < CrewAbilityPockets.threshold_take_dis then
			if CrewAbilityPockets:transfer_items_from_teamAI_to_unit(self._unit, other_unit, false) > 0 then
				if not self._unit:sound():speaking() then
					self._unit:sound():say('g15', true, false)
				end
				return 'pocketed'
			end
		end
	end

	return cap_original_teamaibrain_onlongdisinteracted(self, amount, other_unit, secondary)
end
