local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cap_original_criminalsmanager_remove = CriminalsManager._remove
function CriminalsManager:_remove(id)
	local character = self._characters[id]
	if character and character.data.ai then
		CrewAbilityPockets:free_items_from_teamAI(character)
	end

	cap_original_criminalsmanager_remove(self, id)

	CrewAbilityPockets:reset_teamAI_panels()
end

function CriminalsManager:character_by_panel_id(panel_id)
	for id, data in pairs(self._characters) do
		if data.taken and data.data.panel_id == panel_id then
			return data
		end
	end
end
