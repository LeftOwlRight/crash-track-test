local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

if Network:is_client() then
	return
end

local cap_original_unitnetworkhandler_syncinteracted = UnitNetworkHandler.sync_interacted
function UnitNetworkHandler:sync_interacted(unit, unit_id, tweak_setting, status, sender)
	if not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return
	end

	local peer = self._verify_sender(sender)
	if not peer then
		return
	end

	local is_c4
	if tweak_setting == 'c4_stash_in_bot_pocket' then
		tweak_setting = 'stash_in_bot_pocket'
		is_c4 = true
	end

	if tweak_setting == 'stash_in_bot_pocket' then
		local int = alive(unit) and unit:interaction()
		if int and int:active() then
			local special_equipment = is_c4 and 'c4' or int._special_equipment or int._tweak_data.special_equipment_block
			if not CrewAbilityPockets:give_item_to_teamAI(special_equipment, peer:unit():position(), true) then
				peer:send('give_equipment', special_equipment, 1, true)
			end
		end
		return

	elseif tweak_setting == 'multi_stash_in_bot_pocket' then
		local int = alive(unit) and unit:interaction()
		if int and int:active() then
			local amount_wanted = status
			local special_equipment = int._special_equipment or 'c4'
			if not CrewAbilityPockets:give_item_to_teamAI(special_equipment, peer:unit():position(), true) then
				peer:send('give_equipment', special_equipment, amount_wanted, true)
			end
			int:sync_interacted(peer, nil, amount_wanted)
		end
		return
	end

	cap_original_unitnetworkhandler_syncinteracted(self, unit, unit_id, tweak_setting, status, sender)
end
