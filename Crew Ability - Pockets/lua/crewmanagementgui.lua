local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.CrewAbilityPockets = _G.CrewAbilityPockets or {}
CrewAbilityPockets._path = ModPath
CrewAbilityPockets.filter_ability = false
CrewAbilityPockets.ability_name = 'crew_pockets'
CrewAbilityPockets.interaction_name = 'cap_retrieve_items'
CrewAbilityPockets.carried_items = {}
CrewAbilityPockets.threshold_give_dis = 1500

function CrewAbilityPockets:check_peer(peer)
	for _, mod in pairs(peer:synced_mods()) do
		if mod.name == 'Crew Ability: Pockets' then
			return true
		end
	end
end

function CrewAbilityPockets.dis(pos1, pos2)
	return mvector3.distance(pos1, pos2) + 5 * math.abs(pos1.z - pos2.z)
end

function CrewAbilityPockets:character_has_item(character, equipment)
	local carried_items = character and self.carried_items[character.data.panel_id]
	return carried_items and carried_items[equipment]
end

function CrewAbilityPockets:get_missing_items_from_character(character)
	local result = {}

	local carried_items = character and self.carried_items[character.data.panel_id]
	if carried_items then
		for equipment in pairs(carried_items) do
			if not managers.player:has_special_equipment(equipment) then
				table.insert(result, equipment)
			end
		end
	end

	return result
end

function CrewAbilityPockets:get_suitable_teamAI(equipment, player_pos, ignore_dis_threshold)
	if not managers.player:has_category_upgrade('team', self.ability_name) then
		return
	end

	local best_bot
	local best_dis = 1000000000000000

	for ukey, record in pairs(managers.groupai:state():all_AI_criminals()) do
		if record.status ~= 'dead' and record.status ~= 'disabled' then
			local character = managers.criminals:character_by_unit(record.unit)
			if character and not self:character_has_item(character, equipment) then
				local pos = record.unit:movement()._m_pos
				local dis = self.dis(player_pos, pos)
				if dis < best_dis then
					if ignore_dis_threshold or dis < self.threshold_give_dis then
						best_dis = dis
						best_bot = character
					end
				end
			end
		end
	end

	return best_bot
end

function CrewAbilityPockets:update_all_interactions()
	for _, record in pairs(managers.groupai:state():all_AI_criminals()) do
		local character = managers.criminals:character_by_unit(record.unit)
		self:update_interaction(character)
	end
end

function CrewAbilityPockets:update_interaction(character)
	if not character then
		return
	end

	local unit = character.unit
	if not alive(unit) or unit:slot() ~= 16 then
		return
	end

	local cdmg = unit:character_damage()
	if not cdmg or not cdmg.is_downed or cdmg:is_downed() or type(cdmg._bleed_out_health) == 'number' then
		return
	end

	local interaction = unit:interaction()
	if interaction.tweak_data ~= self.interaction_name then
		interaction:set_tweak_data(self.interaction_name)
	end

	local missing_items = self:get_missing_items_from_character(character)
	local active = #missing_items > 0
	if active ~= interaction:active() then
		if not active then
			interaction:unselect()
		end
		interaction:set_active(active, false)
	end
end

function CrewAbilityPockets:add_item_to_teamAI_hud(character, equipment)
	if not character or not character.data.ai then
		return
	end

	local panel_id = character.data.panel_id

	if managers.hud and tweak_data.equipments.specials[equipment] then
		managers.hud:add_teammate_special_equipment(panel_id, {
			id = equipment,
			icon = tweak_data.equipments.specials[equipment].icon,
		})
	end

	if Network:is_server() then
		LuaNetworking:SendToPeers('PocketIn', character.name .. ' ' .. equipment)
	else
		self.carried_items[panel_id] = self.carried_items[panel_id] or {}
		self.carried_items[panel_id][equipment] = true
	end

	self:update_interaction(character)
end

function CrewAbilityPockets:remove_item_from_teamAI_hud(character, equipment)
	if not character or not character.data.ai then
		return
	end

	local panel_id = character.data.panel_id

	if managers.hud then
		managers.hud:remove_teammate_special_equipment(panel_id, equipment)
	end

	if Network:is_server() then
		LuaNetworking:SendToPeers('PocketOut', character.name .. ' ' .. equipment)
	else
		self.carried_items[panel_id] = self.carried_items[panel_id] or {}
		self.carried_items[panel_id][equipment] = nil
	end

	self:update_interaction(character)
end

function CrewAbilityPockets:reset_teamAI_panels()
	if Network:is_client() then
		return
	end

	for panel_id, items in pairs(self.carried_items) do
		local character = managers.criminals:character_by_panel_id(panel_id)
		for equipment in pairs(items) do
			self:remove_item_from_teamAI_hud(character, equipment)
			self:add_item_to_teamAI_hud(character, equipment)
		end
	end
end

function CrewAbilityPockets:give_item_to_teamAI(equipment, player_pos, ignore_dis_threshold)
	if not equipment then
		return false
	end

	local td = tweak_data.equipments.specials[equipment]
	if td and td.avoid_tranfer then
		return false
	end

	local character = self:get_suitable_teamAI(equipment, player_pos, ignore_dis_threshold)
	if not character then
		return false
	end

	local panel_id = character.data.panel_id
	self.carried_items[panel_id] = self.carried_items[panel_id] or {}
	self.carried_items[panel_id][equipment] = true

	if not character.unit:sound():speaking() then
		character.unit:sound():say('g92', true, false)
	end

	self:add_item_to_teamAI_hud(character, equipment)

	return character
end

function CrewAbilityPockets:transfer_items_from_teamAI_to_unit(bot_unit, player_unit, forced)
	if not alive(player_unit) then
		return 0
	end

	local peer = managers.network:session():peer_by_unit(player_unit)

	return self:transfer_items_from_teamAI_to_peer(bot_unit, peer, forced)
end

function CrewAbilityPockets:transfer_items_from_teamAI_to_peer(bot_unit, peer, forced)
	if not peer or not alive(bot_unit) then
		return 0
	end

	local character = managers.criminals:character_by_unit(bot_unit)
	if not character then
		return 0
	end

	local bot_items = self.carried_items[character.data.panel_id]
	if not bot_items then
		return 0
	end

	local peer_id = peer:id()
	local player_items = managers.player:get_synced_equipment_possession(peer_id)
	local amount = 0

	for equipment in pairs(bot_items) do
		if forced or not player_items or not player_items[equipment] or player_items[equipment] == 0 then
			amount = amount + 1
			bot_items[equipment] = nil
			self:remove_item_from_teamAI_hud(character, equipment)

			local amount_se = tweak_data.equipments.specials[equipment] and tweak_data.equipments.specials[equipment].quantity or 1

			if peer_id == 1 then
				managers.player:add_special({
					transfer = true,
					name = equipment,
					amount = amount_se
				})
			else
				peer:send('give_equipment', equipment, amount_se, true)
			end
		end
	end

	return amount
end

function CrewAbilityPockets:free_items_from_teamAI(character)
	if Network:is_client() then
		return
	end

	if not character then
		return
	end

	local bot_items = self.carried_items[character.data.panel_id]
	if not bot_items then
		return
	end

	local fake_possessions = {}
	for equipment in pairs(bot_items) do
		fake_possessions[equipment] = 1
		self:remove_item_from_teamAI_hud(character, equipment)
	end

	local fake_peer_id = CriminalsManager.MAX_NR_CRIMINALS + 1 -- let's assume and don't verify anything, yolo.
	managers.player._global.synced_equipment_possession[fake_peer_id] = fake_possessions

	managers.player:transfer_special_equipment(fake_peer_id, managers.groupai:state():num_alive_players() == 0)

	managers.player._global.synced_equipment_possession[fake_peer_id] = nil
	self.carried_items[character.data.panel_id] = nil
end

function CrewAbilityPockets:setup()
	local cap_original_hostnetworksession_sendtopeerssynched = HostNetworkSession.send_to_peers_synched
	function HostNetworkSession:send_to_peers_synched(typ, ...)
		if typ == 'set_unit' then
			local params = {...}
			local loadout = params[3]
			if loadout:find(CrewAbilityPockets.ability_name) then
				local patched_loadout = loadout:gsub(CrewAbilityPockets.ability_name, 'nil')
				for peer_id, peer in pairs(self._peers) do
					params[3] = CrewAbilityPockets:check_peer(peer) and loadout or patched_loadout
					peer:send_queued_sync(typ, unpack(params))
				end
				return
			end
		end

		cap_original_hostnetworksession_sendtopeerssynched(self, typ, ...)
	end

	CrewAbilityPockets.setup = function() end
end

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_CrewAbilityPockets', function(loc)
	for _, filename in pairs(file.GetFiles(CrewAbilityPockets._path .. 'loc/')) do
		local str = filename:match('^(.*).txt$')
		if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
			loc:load_localization_file(CrewAbilityPockets._path .. 'loc/' .. filename)
			break
		end
	end
	loc:load_localization_file(CrewAbilityPockets._path .. 'loc/english.txt', false)
end)

local cap_original_crewmanagementgui_populatecustom = CrewManagementGui.populate_custom
function CrewManagementGui:populate_custom(category, henchman_index, tweak, list, ...)
	if category == 'ability' and not table.contains(list, CrewAbilityPockets.ability_name) then
		table.insert(list, CrewAbilityPockets.ability_name)
	end
	return cap_original_crewmanagementgui_populatecustom(self, category, henchman_index, tweak, list, ...)
end

DB:create_entry(
	Idstring('texture'),
	Idstring('guis/dlcs/mom/textures/pd2/ai_abilities_cap'),
	ModPath .. 'assets/pockets.dds'
)

tweak_data.hud_icons[CrewAbilityPockets.ability_name] = {
	texture_rect = { 0, 0, 128, 128 },
	texture = 'guis/dlcs/mom/textures/pd2/ai_abilities_cap'
}

tweak_data.upgrades.crew_ability_definitions[CrewAbilityPockets.ability_name] = {
	icon = CrewAbilityPockets.ability_name,
	name_id = 'menu_crew_pockets'
}

tweak_data.upgrades.values.team[CrewAbilityPockets.ability_name] = { true }

Hooks:Add('NetworkReceivedData', 'NetworkReceivedData_CrewAbilityPockets', function(sender, messageType, data)
	if sender == 1 then
		if messageType == 'PocketIn' then
			local name, equipment = data:match('(%a+) (.+)')
			CrewAbilityPockets:add_item_to_teamAI_hud(managers.criminals:character_by_name(name), equipment)
		elseif messageType == 'PocketOut' then
			local name, equipment = data:match('(%a+) (.+)')
			CrewAbilityPockets:remove_item_from_teamAI_hud(managers.criminals:character_by_name(name), equipment)
		end
	elseif messageType == 'PocketTakeFrom' then
		local name = data
		local character = managers.criminals:character_by_name(name)
		local peer = managers.network:session():peer(sender)
		if character and peer then
			CrewAbilityPockets:transfer_items_from_teamAI_to_peer(character.unit, peer, false)
		end
	end
end)
