local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

DelayedCalls:Add('DelayedMod_CAP_PlayerStandard', 0, function()
	local cap_original_playerstandard_getintimidationaction = PlayerStandard._get_intimidation_action
	function PlayerStandard:_get_intimidation_action(prime_target, char_table, amount, primary_only, detect_only, secondary)
		if secondary or detect_only then
			-- qued
		elseif Network:is_client() then
			-- qued
		elseif prime_target and (prime_target.unit:slot() == 16 or prime_target.unit:slot() == 24 and prime_target.unit:base().kpr_awaken) then
			local csn = self._unit:movement():current_state_name()
			if csn == 'arrested' or csn == 'bleed_out' or csn == 'fatal' or csn == 'incapacitated' then
				-- qued
			elseif not prime_target.unit:brain().on_long_dis_interacted then
				-- qued
			elseif prime_target.unit:brain():on_long_dis_interacted(0, self._unit, secondary) == 'pocketed' then
				if not self:_is_using_bipod() then
					self:_play_distance_interact_redirect(TimerManager:game():time(), 'cmd_come')
				end
				return
			end
		end

		return cap_original_playerstandard_getintimidationaction(self, prime_target, char_table, amount, primary_only, detect_only, secondary)
	end
end)
