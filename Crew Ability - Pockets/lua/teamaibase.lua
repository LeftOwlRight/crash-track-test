local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cap_original_teamaibase_save = TeamAIBase.save
function TeamAIBase:save(data)
	local original_ability = self._loadout.ability
	if original_ability == CrewAbilityPockets.ability_name and CrewAbilityPockets.filter_ability then
		self._loadout.ability = nil
	end

	cap_original_teamaibase_save(self, data)

	self._loadout.ability = original_ability
end

local cap_original_teamaibase_setloadout = TeamAIBase.set_loadout
function TeamAIBase:set_loadout(loadout)
	if loadout.ability == CrewAbilityPockets.ability_name then
		CrewAbilityPockets:setup()
	end

	cap_original_teamaibase_setloadout(self, loadout)
end
