local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cap_original_hostnetworksession_chkdropinpeer = HostNetworkSession.chk_drop_in_peer
function HostNetworkSession:chk_drop_in_peer(dropin_peer)
	if dropin_peer:expecting_dropin() and not CrewAbilityPockets:check_peer(dropin_peer) then
		CrewAbilityPockets.filter_ability = true
	end

	local result = cap_original_hostnetworksession_chkdropinpeer(self, dropin_peer)

	CrewAbilityPockets.filter_ability = false

	return result
end
