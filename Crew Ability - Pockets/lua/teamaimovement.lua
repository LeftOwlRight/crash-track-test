local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function TeamAIMovement:downed()
	local interaction = self._unit:interaction()
	if Network:is_server() then
		return interaction._active and interaction.tweak_data == 'revive'
	else
		return self._unit:interaction()._active
	end
end
