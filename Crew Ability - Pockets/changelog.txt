Revision 14:
- updated to U222

Revision 13:
- added spanish translation by Kilowide

Revision 12:
- fixed wrong color of bot contour staying on body after taking items from bot

Revision 11:
- fixed wrong message when a bot revive another bot

Revision 10:
- fixed patch of revive interaction wrongfully applied to players too

Revision 9:
- fixed revive interaction broken after a bot got cloaked

Revision 8:
- replaced how items retrieving is done by a proper interaction with a message on screen when available

Revision 7:
- fixed interrupted interactions not handled at all
- fixed some item duplication issues
- fixed keycard lost when given to bots by client
- fixed C4 when given to bots by client
- fixed bot pocket availability working only partially for clients

Revision 6:
- added support for C4 (ex: Panic Room, Golden Grin Casino, Beneath the Moutain)

Revision 5:
- fixed an interaction conflict with bots idling during stealth

Revision 4:
- fixed a crash when calling a joker

Revision 3:
- added chinese translation by LR_Daring

Revision 2:
- fixed crash due to using mugshot_id instead of panel_id
- fixed hud not properly refreshed for bots when player drops in
- changed sync message format to use character names

Revision 1:
- initial release