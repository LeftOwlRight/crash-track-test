{
	"blt_version" : 2,
	"name" : "Crew Ability: Pockets",
	"description" : "Bots can carry items like keycards.",
	"author" : "TdlQ\n    chinese translation by LR_Daring\n    spanish translation by Kilowide",
	"image" : "tdlq.dds",
	"color" : "0.52 1 0",
	"contact" : "",
	"version" : "14",
	"simple_update_url" : "http://pd2mods.z77.fr/update/CrewAbilityPockets.zip",
	"updates": [
		{
			"identifier" : "SimpleModUpdater",
			"display_name" : "Simple Mod Updater",
			"install_folder" : "Simple Mod Updater",
			"host" : { "meta": "http://pd2mods.z77.fr/meta/SimpleModUpdater", }
		}
	],
	"hooks" : [
		{
			"hook_id" : "lib/managers/criminalsmanager",
			"script_path" : "lua/criminalsmanager.lua"
		},
		{
			"hook_id" : "lib/managers/menu/crewmanagementgui",
			"script_path" : "lua/crewmanagementgui.lua"
		},
		{
			"hook_id" : "lib/network/base/hostnetworksession",
			"script_path" : "lua/hostnetworksession.lua"
		},
		{
			"hook_id" : "lib/network/handlers/unitnetworkhandler",
			"script_path" : "lua/unitnetworkhandler.lua"
		},
		{
			"hook_id" : "lib/units/interactions/interactionext",
			"script_path" : "lua/interactionext.lua"
		},
		{
			"hook_id" : "lib/units/player_team/teamaibase",
			"script_path" : "lua/teamaibase.lua"
		},
		{
			"hook_id" : "lib/units/player_team/teamaimovement",
			"script_path" : "lua/teamaimovement.lua"
		}
	]
}
