{
	"menu_crew_pockets" : "Pockets",
	"menu_crew_pockets_desc" : "Your team AI can carry special items like chemicals, keycards, planks, etc.\n\nWhen players try to pick up an item they already have, it will be given to the closest AI having room for it.\n\nAI will give items back when called at a short distance by the player followed.\n\nNB: if the AI holding the ability goes into custody, remaining AI will keep all their items but they won't accept to carry new ones.",

	"cap_hud_interact_take_from_teammate" : "Press $BTN_INTERACT to take special items from teammate"
}
