if not UsefulBots then
  return
end

function TeamAILogicIdle.is_valid_intimidation_target(unit, unit_tweak, unit_anim, unit_damage, data, distance)
  if UsefulBots.settings.dominate_enemies > 2 then
    return false
  end
  if SuperSeriousShooter and not managers.groupai:state()._rescue_allowed then
    return false
  end
  if unit:unit_data().disable_shout then
    return false
  end
  local surrender = unit_tweak.surrender
  if not surrender or surrender == tweak_data.character.presets.surrender.special or unit_anim.hands_tied then
    return false
  end
  local surrender_window = unit:brain()._logic_data.surrender_window
  if surrender_window and TimerManager:game():time() > surrender_window.window_expire_t then
    return false
  end
  local intimidate_range_enemies = tweak_data.player.long_dis_interaction.intimidate_range_enemies
  if distance > intimidate_range_enemies then
    return false
  end
  if unit_anim.hands_back or unit_anim.surrender then
    return true
  end
  if not managers.groupai:state():has_room_for_police_hostage() then
    return false
  end
  if surrender_window then
    return true
  end
  if UsefulBots.settings.dominate_enemies > 1 then
    return false
  end
  if distance > intimidate_range_enemies * 0.75 then
    return false
  end
  local health_max = 0
  local surrender_health = surrender.reasons and surrender.reasons.health or surrender.factors and surrender.factors.health or {}
  for k, _ in pairs(surrender_health) do
    health_max = k > health_max and k or health_max
  end
  if unit_damage:health_ratio() > health_max / 5 then
    return false
  end
  local num = 0
  local max = 2 + table.count(managers.groupai:state():all_char_criminals(), function (u_data) return u_data == "dead" end) * 2
  local dis = intimidate_range_enemies * 1.5
  for _, v in pairs(data.detected_attention_objects) do
    local u_damage = v.unit and v.unit.character_damage and v.unit:character_damage()
    if v.verified and v.unit ~= unit and v.dis < dis and u_damage and not u_damage:dead() then
      num = num + 1
      if num > max then
        return false
      end
    end
  end
  return true
end
