Hooks:PostHook(WeaponFactoryTweakData, "init", "TactVGPackModInit", function(self)
local vg_ids = {"wpn_fps_upg_vg_fab_reg","wpn_fps_upg_vg_gps02","wpn_fps_upg_vg_gps02bipod","wpn_fps_upg_vg_tangodown","wpn_fps_upg_vg_troy_modular","wpn_fps_upg_vg_troy_modularshort","wpn_fps_upg_vg_utg_combat","wpn_fps_upg_vg_utg_combatfl"}
local wpn_ovr_ids = {"wpn_fps_smg_mp7","wpn_fps_smg_x_mp7","wpn_fps_ass_tecci"}
for _, wpn_id in pairs(wpn_ovr_ids) do
	if not self[wpn_id].override then
		self[wpn_id].override = {}
	end
end
--
for _, vg_id in pairs(vg_ids) do
	self.wpn_fps_ass_tecci.override[vg_id] = {a_obj = "a_vg_ok"}
end
for _, vg_id in pairs(vg_ids) do
	self.wpn_fps_smg_mp7.override[vg_id] = {forbids = {"wpn_fps_upg_vg_ass_smg_stubby"}}
	self.wpn_fps_smg_x_mp7.override[vg_id] = {forbids = {"wpn_fps_upg_vg_ass_smg_stubby"}}
end
	self.parts.wpn_fps_smg_polymer_fg_standard.type = "vertical_grip"
end)