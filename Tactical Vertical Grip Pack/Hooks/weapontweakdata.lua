Hooks:PostHook(WeaponTweakData, "init", "TactVGPackModInit", function(self)
if self.SetupAttachmentPoint then
	self:SetupAttachmentPoint("polymer", {
		name = "a_vg",
		base_a_obj = "a_fg",
		position = Vector3(0, 1.8, 0)
	})
	self:SetupAttachmentPoint("x_polymer", {
		name = "a_vg",
		base_a_obj = "a_fg",
		position = Vector3(0, 1.8, 0)
	})
	self:SetupAttachmentPoint("tecci", {
		name = "a_vg_ok",
		base_a_obj = "a_vg",
		position = Vector3(0, 2, 0.3)
	})
end
end)