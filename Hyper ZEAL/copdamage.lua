﻿--Adds visor shattering
local ids_func = Idstring
local table_contains = table.contains

local shatter_enemies = {
	ids_func("units/pd2_dlc_gitgud/characters/ene_zeal_tazer/ene_zeal_tazer"),
	ids_func("units/pd2_dlc_gitgud/characters/ene_zeal_tazer/ene_zeal_tazer_husk")
}

Hooks:PreHook( CopDamage, "_spawn_head_gadget", "smash_HH_ZEAL", function(self, params)
	if not self._head_gear then
		return
	end

	local my_unit = self._unit

	if not table_contains(shatter_enemies, my_unit:name()) then
		return
	end
	
	local head_obj = ids_func("Head")
	local head_object_get = my_unit:get_object(head_obj)
	
	if not head_object_get then
		return
	end
	
	local world_g = World		
	local sound_ext = my_unit:sound()	
	
	world_g:effect_manager():spawn({
		effect = ids_func("effects/particles/bullet_hit/glass_breakable/bullet_hit_glass_breakable"),
		parent = head_object_get		
	})			
	
	sound_ext:play("swat_heavy_visor_shatter", nil, nil)
	sound_ext:play("swat_heavy_visor_shatter", nil, nil)
	sound_ext:play("swat_heavy_visor_shatter", nil, nil)
end)
