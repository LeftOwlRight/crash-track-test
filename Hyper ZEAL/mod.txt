{
	"name": "Hyper ZEAL",
	"description": "Hyper Heisting's ZEAL units but as a standalone mod",
	"author": "Designs by fugsystem, Reno, and Jarey, made into a standalone by IareAwesome17",
	"contact": "",
	"version": "1.1",
	"color": "0 1 1",
	"image": "zeal_rifle.png",
	"hooks": [
		{"hook_id": "lib/units/enemies/cop/copdamage", "script_path": "copdamage.lua"}
	]
}