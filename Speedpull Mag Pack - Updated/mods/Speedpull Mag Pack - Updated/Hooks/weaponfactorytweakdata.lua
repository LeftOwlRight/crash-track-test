Hooks:PostHook( WeaponFactoryTweakData, "init", "magpul_packInit", function(self)

-- Assault Rifle --
self.parts.wpn_fps_ass_s552_m_quick.adds = {"wpn_fps_ass_s552_m_magpul"}

self.parts.wpn_fps_ass_scar_m_quick.adds = {"wpn_fps_ass_scar_m_magpul"}

self.parts.wpn_fps_ass_famas_m_quick.adds = {"wpn_fps_ass_famas_m_magpul"}

self.parts.wpn_fps_ass_vhs_m_quick.adds = {"wpn_fps_ass_vhs_m_magpul"}

self.parts.wpn_fps_ass_asval_m_quick.adds = {"wpn_fps_ass_asval_m_magpul"}

self.parts.wpn_fps_ass_galil_m_quick.adds = {"wpn_fps_ass_galil_m_magpul"}

self.parts.wpn_fps_ass_fal_m_quick.adds = {"wpn_fps_ass_fal_m_magpul"}

self.parts.wpn_fps_ass_g3_m_quick.adds = {"wpn_fps_ass_g3_m_magpul"}

-- Shotgun --
self.parts.wpn_fps_shot_saiga_m_quick.adds = {"wpn_fps_shot_saiga_m_magpul"}
self.parts.wpn_fps_shot_x_saiga_m_quick.adds = {"wpn_fps_shot_saiga_m_magpul"}

self.parts.wpn_fps_sho_aa12_m_quick.adds = {"wpn_fps_sho_aa12_m_magpul"}

self.parts.wpn_fps_sho_rota_m_quick.adds = {"wpn_fps_sho_rota_m_magpul"}
self.parts.wpn_fps_sho_x_rota_m_quick.adds = {"wpn_fps_sho_rota_m_magpul"}

-- Pistol --
self.parts.wpn_fps_pis_g17_m_quick.adds = {"wpn_fps_pis_g17_m_magpul"}
self.parts.wpn_fps_pis_x_g17_m_quick.adds = {"wpn_fps_pis_g17_m_magpul"}

self.parts.wpn_fps_pis_g26_m_quick.adds = {"wpn_fps_pis_g17_m_magpul"}
self.parts.wpn_fps_pis_g26_m_quick.override_weapon_add = {CLIP_AMMO_MAX=7}
self.parts.wpn_fps_jowi_m_quick.adds = {"wpn_fps_pis_g17_m_magpul"}
self.parts.wpn_fps_jowi_m_quick.override_weapon_add = {CLIP_AMMO_MAX=14}

self.parts.wpn_fps_pis_beretta_m_quick.adds = {"wpn_fps_pis_beretta_m_magpul"}
self.parts.wpn_fps_pis_x_beretta_m_quick.adds = {"wpn_fps_pis_beretta_m_magpul"}

self.parts.wpn_fps_pis_ppk_m_quick.adds = {"wpn_fps_pis_ppk_m_magpul"}
self.parts.wpn_fps_pis_x_ppk_m_quick.adds = {"wpn_fps_pis_ppk_m_magpul"}

-- SMG --
self.parts.wpn_fps_smg_mp5_m_quick.adds = {"wpn_fps_smg_mp5_m_magpul"}
self.parts.wpn_fps_smg_x_mp5_m_quick.adds = {"wpn_fps_smg_mp5_m_magpul"}

self.parts.wpn_fps_smg_mp9_m_quick.override = {wpn_fps_smg_mp9_m_short = {unit = "units/payday2/weapons/wpn_fps_smg_mp9_pts/wpn_fps_smg_mp9_m_extended"}}
self.parts.wpn_fps_smg_mp9_m_quick.adds = {"wpn_fps_smg_mp9_m_magpul"}
self.parts.wpn_fps_smg_mp9_m_quick.override_weapon_add = {CLIP_AMMO_MAX=12}
self.parts.wpn_fps_smg_x_mp9_m_quick.override = {wpn_fps_smg_mp9_m_short = {unit = "units/payday2/weapons/wpn_fps_smg_mp9_pts/wpn_fps_smg_mp9_m_extended"}}
self.parts.wpn_fps_smg_x_mp9_m_quick.adds = {"wpn_fps_smg_mp9_m_magpul"}
self.parts.wpn_fps_smg_x_mp9_m_quick.override_weapon_add = {CLIP_AMMO_MAX=24}

self.parts.wpn_fps_smg_shepheard_m_quick.adds = {"wpn_fps_smg_shepheard_m_magpul"}
self.parts.wpn_fps_smg_x_shepheard_m_quick.adds = {"wpn_fps_smg_shepheard_m_magpul"}

self.parts.wpn_fps_smg_erma_m_quick.adds = {"wpn_fps_smg_erma_m_magpul"}
self.parts.wpn_fps_smg_x_erma_m_quick.adds = {"wpn_fps_smg_erma_m_magpul"}

self.parts.wpn_fps_smg_thompson_m_quick.adds = {"wpn_fps_smg_thompson_m_magpul"}
self.parts.wpn_fps_smg_x_thompson_m_quick.adds = {"wpn_fps_smg_thompson_m_magpul"}

self.parts.wpn_fps_smg_m45_m_quick.adds = {"wpn_fps_smg_m45_m_magpul"}
self.parts.wpn_fps_smg_x_m45_m_quick.adds = {"wpn_fps_smg_m45_m_magpul"}

self.parts.wpn_fps_smg_mp7_m_quick.override = {wpn_fps_smg_mp7_m_short = {unit = "units/pd2_dlc_dec5/weapons/wpn_fps_smg_mp7_pts/wpn_fps_smg_mp7_m_extended"}}
self.parts.wpn_fps_smg_mp7_m_quick.adds = {"wpn_fps_smg_mp7_m_magpul"}
self.parts.wpn_fps_smg_mp7_m_quick.override_weapon_add = {CLIP_AMMO_MAX=12}
self.parts.wpn_fps_smg_x_mp7_m_quick.override = {wpn_fps_smg_mp7_m_short = {unit = "units/pd2_dlc_dec5/weapons/wpn_fps_smg_mp7_pts/wpn_fps_smg_mp7_m_extended"}}
self.parts.wpn_fps_smg_x_mp7_m_quick.adds = {"wpn_fps_smg_mp7_m_magpul"}
self.parts.wpn_fps_smg_x_mp7_m_quick.override_weapon_add = {CLIP_AMMO_MAX=24}

self.parts.wpn_fps_smg_cobray_m_quick.adds = {"wpn_fps_smg_cobray_m_magpul"}
self.parts.wpn_fps_smg_x_cobray_m_quick.adds = {"wpn_fps_smg_cobray_m_magpul"}

self.parts.wpn_fps_smg_scorpion_m_quick.adds	= {"wpn_fps_smg_scorpion_m_magpul"}
self.parts.wpn_fps_smg_x_scorpion_m_quick.adds	= {"wpn_fps_smg_scorpion_m_magpul"}

self.parts.wpn_fps_smg_baka_m_quick.adds = {"wpn_fps_smg_baka_m_magpul"}
self.parts.wpn_fps_smg_x_baka_m_quick.adds = {"wpn_fps_smg_baka_m_magpul"}

self.parts.wpn_fps_smg_schakal_m_quick.adds = {"wpn_fps_smg_schakal_m_magpul"}
self.parts.wpn_fps_smg_x_schakal_m_quick.adds = {"wpn_fps_smg_schakal_m_magpul"}

self.parts.wpn_fps_smg_tec9_m_quick.adds = {"wpn_fps_smg_tec9_m_magpul"}
self.parts.wpn_fps_smg_x_tec9_m_quick.adds = {"wpn_fps_smg_tec9_m_magpul"}

self.parts.wpn_fps_smg_polymer_m_quick.adds = {"wpn_fps_smg_polymer_m_magpul"}
self.parts.wpn_fps_smg_x_polymer_m_quick.adds = {"wpn_fps_smg_polymer_m_magpul"}

self.parts.wpn_fps_smg_coal_m_quick.adds = {"wpn_fps_smg_coal_m_magpul"}
self.parts.wpn_fps_smg_x_coal_m_quick.adds = {"wpn_fps_smg_coal_m_magpul"}

self.parts.wpn_fps_smg_sterling_m_quick.adds = {"wpn_fps_smg_sterling_m_magpul"}
self.parts.wpn_fps_smg_x_sterling_m_quick.adds = {"wpn_fps_smg_sterling_m_magpul"}

self.parts.wpn_fps_smg_uzi_m_quick.adds = {"wpn_fps_smg_uzi_m_magpul"}
self.parts.wpn_fps_smg_x_uzi_m_quick.adds = {"wpn_fps_smg_uzi_m_magpul"}

end )