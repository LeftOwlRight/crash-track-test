Hooks:PostHook( CopDamage , "die" , "PostCopDamageDie" , function( self , attack_data )

    if Global.level_data.level_id == "spa" then
    	return
    end

	self._unit:contour():remove( "friendly" , true )
	self._unit:contour():remove( "highlight_character" , true )

end )