Hooks:PostHook( HuskCopDamage , "die" , "PostHuskCopDamageDie" , function( self , variant )

    if Global.level_data.level_id == "spa" then
    	return
    end

	self._unit:contour():remove( "friendly" , true )
	self._unit:contour():remove( "highlight_character" , true )

end )