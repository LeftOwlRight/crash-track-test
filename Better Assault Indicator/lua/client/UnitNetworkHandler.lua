BAI:Hook(GroupAIStateBase, "set_whisper_mode", function(self, enabled, ...)
    if not enabled and not BAI._cache.dropin then
        BAI:UpdateAssaultState("control")
    end
end)