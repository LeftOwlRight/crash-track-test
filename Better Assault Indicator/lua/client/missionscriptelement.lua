local heists =
{
    ["rvd2"] = 100023,
    ["red2"] = 101685,
    ["vit"] = 102042
}
local requested_id = heists[Global.game_settings.level_id]
local _f_on_executed = MissionScriptElement.client_on_executed
function MissionScriptElement:client_on_executed()
    _f_on_executed(self)
    if self._id == requested_id then
        managers.hud:SetEndlessClient(true)
    end
end