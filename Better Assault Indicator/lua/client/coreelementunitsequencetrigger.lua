if core then
    core:module("CoreElementUnitSequenceTrigger")
    core:import("CoreMissionScriptElement")
    core:import("CoreCode")
end

local heists =
{
    ["glace"] = 100131,
    ["peta2"] = 101723,
    ["hox_2"] = 101890,
    ["ukrainian_job"] = 101371
}
local override = true
if Global.game_settings.level_id ~= "hox_2" and Global.game_settings.level_id ~= "ukrainian_job" then
    override = false
end
local requested_id = heists[Global.game_settings.level_id]
local dont_trigger_again = false
function ElementUnitSequenceTrigger:client_on_executed(...) --Added new function to ElementUnitSequenceTrigger
    if not dont_trigger_again then
        if self._id == requested_id then
            dont_trigger_again = true
            managers.hud:SetEndlessClient(override)
        end
    end
end