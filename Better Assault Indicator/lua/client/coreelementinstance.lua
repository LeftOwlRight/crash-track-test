if core then
    core:module("CoreElementInstance")
    core:import("CoreMissionScriptElement")
end

local heists =
{
    ["born"] = 100720,
    ["dah"] = 104949,
    ["man"] = 102754,
    ["jolly"] = 100781,
    ["dinner"] = 104979,
    ["kenaz"] = 100379
}
local level_id = Global.game_settings.level_id
local override = level_id ~= "dinner"
local requested_id = heists[level_id]
local _f_on_executed = ElementInstanceOutputEvent.client_on_executed
local dont_trigger_again = false
function ElementInstanceOutputEvent:client_on_executed(...)
    _f_on_executed(self, ...)
    if not dont_trigger_again then
        if self._id == requested_id then
            dont_trigger_again = true
            managers.hud:SetEndlessClient(override)
        end
    end
end