if core then
    core:module("CoreElementCounter")
    core:import("CoreMissionScriptElement")
    core:import("CoreClass")
end

local heists =
{
    ["mad"] = 100523,
    ["sah"] = 101177
}
local requested_id = heists[Global.game_settings.level_id]
local dont_trigger_again = false
function ElementCounter:client_on_executed(...)
    if not dont_trigger_again then
        if self._id == requested_id and self._values.counter_target == 2 then
            dont_trigger_again = true
            managers.hud:SetEndlessClient(true)
        end
    end
end