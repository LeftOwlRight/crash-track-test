local BAI = BAI
if BAI:IsOr(BAI._cache.level_id, "Enemy_Spawner", "enemy_spawner2") then
    return
end

local cache = BAI._cache

local function phase(self)
    if self._task_data.assault.phase ~= "anticipation" then
        BAI:UpdateAssaultState(self._task_data.assault.phase)
    end
end

function GroupAIStateBase:GetAssaultState()
    return self._task_data.assault.phase
end

if cache.level_id == "pbr2" and BAI.IsHost then
    BAI:Hook(GroupAIStateBesiege, "_upd_recon_tasks", function(self)
        if self._task_data.recon.tasks and self._task_data.recon.tasks[1] then
            BAI:UpdateAssaultState("control", true, true)
            BAI:Unhook(nil, "_upd_recon_tasks")
        end
    end)
else
    BAI:Hook(GroupAIStateBase, "on_enemy_weapons_hot", function(self)
        BAI:UpdateAssaultState("control", true, true)
    end)
end

BAI:Hook(HUDManager, "sync_start_anticipation_music", function(self)
    BAI:UpdateAssaultState("anticipation")
end)

function BAI:UpdateAssaultState(state, stealth_broken, no_as_mod)
    if state and cache._assault_state ~= state and self:IsNot(cache.AssaultType, self.Enum.AssaultType.Endless, self.Enum.AssaultType.Captain, self.Enum.AssaultType.NoReturn) then
        cache._assault_state = state
        self:CallEvent(self.EventList.AssaultStateChange, state, stealth_broken, no_as_mod)
    end
end

function BAI:UpdateAssaultStateOverride(state, override)
    if state and self:IsNot(cache.AssaultType, self.Enum.AssaultType.Endless, self.Enum.AssaultType.Captain, self.Enum.AssaultType.NoReturn) then
        cache._assault_state = state
        self:CallEvent(self.EventList.AssaultStateChangeOverride, state, override)
    end
end

function BAI:SetAssaultStatesHook(hook)
    if hook then
        self:Hook(GroupAIStateBesiege, "_upd_assault_task", phase)
    else
        self:Unhook(nil, "_upd_assault_task")
    end
end

BAI:SetAssaultStatesHook(true)