local _f_set_dropin = ConnectionNetworkHandler.set_dropin
function ConnectionNetworkHandler:set_dropin()
    _f_set_dropin(self)
    BAI._cache.dropin = true
end