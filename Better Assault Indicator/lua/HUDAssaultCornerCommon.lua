local BAI = BAI
function HUDAssaultCorner:ApplyHooks()
    if not self.applied then
        dofile(BAI.LuaPath .. "assault_states.lua")
        dofile(BAI.LuaPath .. "localizationmanager.lua")
        managers.localization:SetVariables(self.is_client)
        if self.is_host and self.assault_extender_modifier then
            managers.localization:CSAE_Activate()
        end
        BAI:ApplyModCompatibility(1)
        self.applied = true
    end
end

function HUDAssaultCorner:InitBAI()
    self.CompatibleHost = false
    self.BAIHost = false
    self.was_endless = false
    self.is_client = BAI.IsClient
    self.is_host = not self.is_client
    self.assault_type = nil
    self.trigger_assault_start_event = true
    self.is_skirmish = BAI._cache.is_skirmish
    self.is_crimespree = BAI._cache.is_crimespree
    if BAI._cache.MutatorAssaultExtender then
        self.assault_extender_modifier = true
    end
    if self.is_client then
        dofile(BAI.ClientPath .. "EnemyManager.lua")
        dofile(BAI.ClientPath .. "UnitNetworkHandler.lua")
        self.endless_client = BAI:EndlessClient()
        if self.is_crimespree then
            self.assault_extender_modifier = managers.crime_spree:DoesServerHasAssaultExtenderModifier()
        end
        if BAI._cache.MutatorEndlessAssaults then
            self:SetEndlessClient(true)
        end
    end
    dofile(BAI.LuaPath .. "GroupAIStateBesiege.lua")
    self.no_endless_assault_override = BAI:IsPlayingHeistWithFakeEndlessAssault()
    BAI.SetVariables = function()
        managers.localization:SetVariables()
    end
    self:ApplyCompatibility()
    self._image_extension = ""
    self:ApplyNewIcons()
    self:SetCompatibilityFlags(BAI._cache.detected_hud)
    self:ApplyHooks()
    if BAI:IsHostagePanelHidden() then
        self:DisableHostagePanelFunctions()
    end
    BAI:LoadCustomText()
    self:UpdateColors()
    self:InitAAIPanel()
    self:InitCaptainPanel()
    self:UpdateAssaultPanelPosition()
end

function HUDAssaultCorner:ApplyCompatibility()
end

function HUDAssaultCorner:ApplyNewIcons()
end

function HUDAssaultCorner:SetCompatibilityFlags(hud_number)
    self.assault_panel_position_disabled = true
    if (self.Vanilla or hud_number == 2) or BAI:IsOr(hud_number, 3, 4, 5, 7, 8) then -- 3 = Void UI; 4 = Sora's HUD Reborn; 5 = HoloUI; 7 = PD:TH HUD Reborn; 8 = Restoration Mod
        self.AAIPanel = not self._v2_corner
        if hud_number == 3 then -- Void UI
            self.AAIFunction = "AAIPanelVoidUI"
            self.AAIFunctionArgs1 = { callback(self, self, "_blink_background") }
            self.AAIFunctionArgs2 = self.AAIFunctionArgs1
        elseif hud_number == 4 then -- Sora's HUD Reborn
            self.AAIFunction = "FadeIn"
            self.AAIFunctionArgs1 = { 1 } -- Alpha
            self.AAIFunctionArgs2 = { 1 }
        elseif hud_number == 7 then -- PD:TH HUD Reborn
            self.AAIFunction = "FadeIn"
            self.AAIFunctionArgs1 = { 1 } -- Alpha
            self.AAIFunctionArgs2 = { 1 }
            self.AAIPanel = false
            self.AAIPanelOverride = true
        else
            self.AnimateHostagesPanel = true
            self._hud_panel:child("hostages_panel"):set_visible(true)
            self._hud_panel:child("hostages_panel"):set_alpha(1)
            self.AAIFunction = "AAIPanel"
            self.AAIFunctionArgs1 = { "_time_bg_box" }
            self.AAIFunctionArgs2 = { "_spawns_bg_box" }
            if hud_number == 2 and not WolfHUD and not VHUDPlus then
                self.assault_panel_position_disabled = false
            end
            if hud_number == 5 then -- 5 = HoloUI
                self.AAIFunction = "AAIPanelHoloUI"
                self.AAIFunctionArgs1[2] = "time_left"
                self.AAIFunctionArgs2[2] = "spawns_left"
            end
        end
    end
    if self.Vanilla or BAI:IsOr(hud_number, 3, 5, 8, 11) then
        BAI:AddEvent(BAI.EventList.AssaultStart, function()
            if BAI:IsHostagePanelHidden("assault") then
                managers.hud._hud_assault_corner:_hide_hostages()
            end
        end, 0.5)
        BAI:AddEvent(BAI.EventList.AssaultEnd, function()
            if BAI:IsHostagePanelVisible() then
                managers.hud._hud_assault_corner:_show_hostages()
            end
        end)
        BAI:AddEvent(BAI.EventList.EndlessAssaultStart, function()
            if BAI:IsHostagePanelHidden("endless") then
                managers.hud._hud_assault_corner:_hide_hostages()
            end
        end, 0.5)
        local function Active(active)
            self:set_hostage_visibility(BAI:IsHostagePanelVisible(active and "captain" or "assault"))
        end
        BAI:AddEvents({BAI.EventList.NormalAssaultOverride, BAI.EventList.Captain}, Active)
    end
end

function HUDAssaultCorner:UpdateColors()
    self._assault_color = self.is_skirmish and BAI:GetColor("holdout") or BAI:GetRightColor("assault")
    self._vip_assault_color = BAI:GetRightColor("captain")
    self._assault_endless_color = BAI:GetRightColor("endless")
    self._assault_survived_color = BAI:GetColor("survived")
    self:UpdatePONRBox()
end

function HUDAssaultCorner:SetImage(image, panel, icon)
end

function HUDAssaultCorner:GetFactionAssaultText(ws)
    if BAI:ShowFSSAI() then
        return "_fss_mod_" .. math.random(3)
    end
    if not BAI:GetOption("faction_assault_text") or ws or self.is_crimespree then
        return ""
    end
    local difficulty, faction = BAI._cache.Difficulty, BAI._cache.Faction
    if faction == "russia" then -- Every mission with Akan (Russian) enemies
        return "_russia"
    elseif Global.game_settings.level_id == "haunted" or faction == "zombie" then -- Safehouse Nightmare and every mission with zombie enemies
        return "_zombie"
    elseif faction == "murkywater" then -- Every mission with murkywater enemies
        return "_murkywater"
    elseif faction == "federales" then -- Every mission with federales (Spanish FBI) enemies
        return "_federales"
    elseif BAI:IsOr(difficulty, "normal", "hard") then -- Normal, Hard
        return ""
    elseif BAI:IsOr(difficulty, "overkill", "overkill_145") then -- Very Hard, OVERKILL
        return "_fbi"
    elseif BAI:IsOr(difficulty, "easy_wish", "overkill_290") then -- Mayhem, Death Wish
        return "_gensec"
    else --sm_wish; Death Sentence
        return "_zeal"
    end
end

function HUDAssaultCorner:_popup_wave_started()
    self:_popup_wave(self:wave_popup_string_start(), tweak_data.screen_colors.skirmish_color) -- Orange
end

function HUDAssaultCorner:_popup_wave_finished()
    self:_popup_wave(self:wave_popup_string_end(), Color(1, 0.12549019607843137, 0.9019607843137255, 0.12549019607843137)) -- Green
end

function HUDAssaultCorner:SetEndlessClient(dont_override)
    self.endless_client = true
    self.dont_override_endless = dont_override
end

function HUDAssaultCorner:GetRisk()
    local difficulty
    if self.is_crimespree or not BAI:GetOption("show_difficulty_name_instead_of_skulls") then
        difficulty = Idstring("risk")
    else
        difficulty = self:GetDifficultyName()
    end
    return difficulty
end

function HUDAssaultCorner:GetDifficultyName()
    if tweak_data ~= nil then
        return tweak_data.difficulty_name_id
    else
        return Idstring("risk") -- Better safe than sorry
    end
end

function HUDAssaultCorner:GetEndlessAssault()
    if not self.no_endless_assault_override then
        if self.is_host and managers.groupai:state():get_hunt_mode() then
            LuaNetworking:SendToPeersExcept(1, BAI.SyncMessage, BAI.data.EA)
            return true
        end -- Returns nil on host
        return self.endless_client
    end
    return false
end

function HUDAssaultCorner:SetCompatibleHost(BAIHost)
    self.CompatibleHost = true
    self.BAIHost = BAIHost
end

function HUDAssaultCorner:set_hostage_visibility(visibility, no_animation)
    if visibility then
        self:_show_hostages(no_animation)
    else
        self:_hide_hostages(no_animation)
    end
end

function HUDAssaultCorner:DisableHostagePanelFunctions()
    self._hud_panel:child("hostages_panel"):set_visible(false)
    self._hud_panel:child("hostages_panel"):set_alpha(0)
    self._show_hostages = function() end
    self._hide_hostages = function() end
end

function HUDAssaultCorner:UpdateAssaultStateOverride_Override(state, override) -- Overriden when needed in HUD
end