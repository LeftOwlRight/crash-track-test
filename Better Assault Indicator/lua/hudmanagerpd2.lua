local BAI = BAI
dofile(BAI.LuaPath .. "compatibility.lua")
local function CheckIfHookIsRegistered(hook_name)
    if Hooks then
        return Hooks._registered_hooks[hook_name]
    end
end

function HUDManager:InitVariables()
    self._assault_mode = "assault"
    if BAI.IsClient then
        local function SetTime5(active)
            if not active then
                self:SetTimeLeft(5)
            end
        end
        BAI:AddEvent(BAI.EventList.Captain, SetTime5)
        if BAI._cache.level_id == "hox_1" then
            local function SetSpawnsLocal()
                self:SetSpawnsLeft(BAI:CalculateSpawnsFromDiff())
            end
            BAI:AddEvent(BAI.EventList.NormalAssaultOverride, SetTime5, nil, 1)
            BAI:AddEvent(BAI.EventList.NormalAssaultOverride, SetSpawnsLocal, nil, 2)
        end
        BAI:AddEvent(BAI.EventList.AssaultStart, function()
            managers.enemy.force_spawned = 0
        end, nil, nil)
    end
    local function EAS()
        BAI._cache.AssaultType = BAI.Enum.AssaultType.Endless
    end
    BAI:AddEvent(BAI.EventList.EndlessAssaultStart, EAS, nil, 100)
    BAI:AddEvent(BAI.EventList.AssaultEnd, function()
        BAI._cache.AssaultType = BAI.Enum.AssaultType.None
    end)
    BAI:AddEvents({BAI.EventList.AssaultStart, BAI.EventList.NormalAssaultOverride}, function()
        BAI._cache.AssaultType = BAI.Enum.AssaultType.Normal
    end, nil, 200)
    if BAI._cache.Multiplayer and BAI.IsHost then
        BAI:AddEvents({BAI.EventList.AssaultStateChange, BAI.EventList.AssaultStateChangeOverride}, function(state)
            if BAI:GetChatOption("enabled") then
                local translation_table =
                {
                    ["control"] = "Control",
                    ["anticipation"] = "Anticipation",
                    ["build"] = "Build",
                    ["sustain"] = "Sustain",
                    ["fade"] = "Fade"
                }
                if BAI:GetChatOption("sync_assault_states") then
                    if BAI:GetChatOption("sync_assault_states_amount") == 1 then -- All Assault States
                        BAI:SendMessage("Current Assault State: " .. translation_table[state])
                    elseif BAI:IsNot(state, "control", "anticipation") then
                        BAI:SendMessage("Current Assault State: " .. translation_table[state])
                    end
                end
                if BAI:GetChatOption("sync_assault_time") and BAI:GetChatOption("sync_assault_time_amount") == 1 and BAI:IsNot(state, "control", "anticipation") then
                    BAI:SendMessage("Assault Time: " .. managers.localization:CalculateTimeLeft() .. " remaining")
                end
            end
        end)
        BAI:AddEvent(BAI.EventList.EndlessAssaultStart, function()
            if BAI:GetChatOption("enabled") and BAI:GetChatOption("sync_assault_states") then
                BAI:SendMessage("Current Assault State: Endless")
            end
        end)
        BAI:AddEvent(BAI.EventList.AssaultStart, function()
            if BAI:GetChatOption("enabled") and BAI:GetChatOption("sync_assault_time") and BAI:GetChatOption("sync_assault_time_amount") == 2 then
                local time = managers.game_play_central:get_heist_timer() + managers.localization:CalculateTimeLeftNoFormat()
                BAI:SendMessage("Assault will end in: " .. self._hud_player_custody:_get_time_text(time))
            end
        end)
    end
end

function HUDManager:GetAssaultMode()
    return self._assault_mode
end

function HUDManager:SetEndlessClient(dont_override)
    self._hud_assault_corner:SetEndlessClient(dont_override)
end

function HUDManager:SetCompatibleHost(BAIHost)
    BAI:SetCompatibleHost(BAIHost)
    self._hud_assault_corner:SetCompatibleHost(BAIHost)
end

function HUDManager:SetNormalAssaultOverride()
    BAI._cache.AssaultType = BAI.Enum.AssaultType.Normal
    BAI:CallEvent(BAI.EventList.NormalAssaultOverride)
end

function HUDManager:UpdateColors()
    self._hud_assault_corner:UpdateColors()
end

function HUDManager:UpdateAssaultColor(color, assault_type)
    self._hud_assault_corner:UpdateAssaultColor(color, assault_type)
end

local _f_sync_set_assault_mode = HUDManager.sync_set_assault_mode
function HUDManager:sync_set_assault_mode(mode)
    if self._assault_mode ~= mode then
        self._assault_vip = mode == "phalanx"
        self._assault_mode = mode
        BAI._cache.AssaultType = BAI.Enum.AssaultType[self._assault_vip and "Captain" or "Normal"]
        BAI:CallEvent(BAI.EventList.Captain, self._assault_vip)
        _f_sync_set_assault_mode(self, mode)
    end
end

local _f_show_point_of_no_return_timer = HUDManager.show_point_of_no_return_timer
function HUDManager:show_point_of_no_return_timer()
    BAI:CallEvent(BAI.EventList.NoReturn, true)
    BAI._cache.AssaultType = BAI.Enum.AssaultType.NoReturn
    _f_show_point_of_no_return_timer(self)
end

local _f_hide_point_of_no_return_timer = HUDManager.hide_point_of_no_return_timer
function HUDManager:hide_point_of_no_return_timer()
    BAI:CallEvent(BAI.EventList.NoReturn, false)
    BAI._cache.AssaultType = BAI.Enum.AssaultType.None
    _f_hide_point_of_no_return_timer(self)
end

function HUDManager:UpdateAssaultStateColor(state, force_update)
    self._hud_assault_corner:UpdateAssaultStateColor(state, force_update)
end

function HUDManager:IsNormalPoliceAssault()
    return self._hud_assault_corner._assault and not self._hud_assault_corner._assault_endless
end

function HUDManager:IsPoliceAssault()
    return self._hud_assault_corner._assault
end

function HUDManager:IsEndlessPoliceAssault()
    return self._hud_assault_corner._assault_endless
end

function HUDManager:IsWaveSurvivedShowed()
    return self._hud_assault_corner.wave_survived
end

function HUDManager:GetAssaultState()
    return BAI._cache._assault_state
end

function HUDManager:IsHost()
    return BAI.IsHost
end

function HUDManager:IsClient()
    return BAI.IsClient
end

function HUDManager:IsSkirmish()
    return BAI._cache.is_skirmish
end

function HUDManager:GetTimeLeft()
    return BAI._cache.client_time_left - TimerManager:game():time()
end

function HUDManager:GetSpawnsLeft()
    return math.floor(BAI._cache.client_spawns_left - managers.enemy.force_spawned)
end

function HUDManager:SetTimeLeft(time)
    if BAI.IsHost then
        return
    end
    BAI._cache.client_time_left = TimerManager:game():time() + time
end

function HUDManager:SetSpawnsLeft(spawns)
    if BAI.IsHost then
        return
    end
    BAI._cache.client_spawns_left = spawns
end

function HUDManager:GetCompatibleHost()
    return BAI.CompatibleHost
end

function HUDManager:GetBAIHost()
    return BAI.BAIHost
end

function HUDManager:SetCaptainBuff(buff)
    if buff < 0 then
        buff = 0
    end
    self._hud_assault_corner:SetCaptainBuff(buff)
end

function HUDManager:StartEndlessAssaultClient(dont_override)
    self:SetEndlessClient(dont_override)
    self._hud_assault_corner:start_assault_callback()
end

-- Used for Debug only
--[[function HUDManager:activate_objective(data)
    self._hud_objectives:activate_objective(data)
    BAI:Log("Objective ID: " .. data.id)
end]]

local function ApplyCompatibility(self, class)
    class = class or "HUDAssaultCorner"
    BAI:ApplyHUDCompatibility(BAI.settings.hud_compatibility)
    if self._hud_assault_corner and self._hud_assault_corner.InitBAI then
        self:InitVariables()
        self._hud_assault_corner:InitBAI()
        BAI:Log("Successfully initialized") --If the mod doesn't crash above, then this is the good sign that something works here
    else
        BAI:Log("Can't execute code in " .. class .. "! Are you sure it wasn't deleted?", BAI.Enum.LogType.Warning)
    end
end

BAI:Hook(HUDManager, "_create_assault_corner", function(self)
    BAI:Init()
    if ArmStatic and CheckIfHookIsRegistered("HUDManagerSetupPlayerInfoHudPD2") then
        Hooks:Add("HUDManagerSetupPlayerInfoHudPD2", "BAI_MUI_setup", function(self)
            ApplyCompatibility(self, "MUIStats")
        end)
        return -- MUI is present, delay BAI execution until the hook is called
    end
    ApplyCompatibility(self)
end)

local _f_sync_start_assault = HUDManager.sync_start_assault
function HUDManager:sync_start_assault(assault_number)
    BAI:SetTimer()
    _f_sync_start_assault(self, assault_number)
end

--[[if HUDListManager then
    local _f_activate_objective = HUDManager.activate_objective
    function HUDManager:activate_objective(data)
        _f_activate_objective(self, data)
        if self._hud_assault_corner.assault_panel_position == 1 then
            managers.hudlist:change_setting("left_list_y", data.amount and 108 or 86)
        end
    end
end]]

dofile(BAI.LuaPath .. "network.lua")