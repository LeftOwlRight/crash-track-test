local AAIPanel =
{
    InTheAssaultBox = 1,
    CustomPanel = 2
}
local BAI = BAI
local BAIFunctionForced = false
function HUDAssaultCorner:ApplyCompatibility()
    self.hudlist = { bai = {} }
    self:QueryHUDListOptions()
    self:QueryBAIOptions()
    self:InitHUDListOptions()
end

function HUDAssaultCorner:QueryHUDListOptions(update)
    self.hudlist.hostages_hidden = true
    if not update then
        self.hudlist.hudlist_enabled = true
    end
end

function HUDAssaultCorner:QueryBAIOptions()
    self.hudlist.bai.assault_panel_position = BAI:GetOption("assault_panel_position")
    self.hudlist.bai.aai_visible = BAI:GetOption("show_advanced_assault_info")
    self.hudlist.bai.aai_panel = BAI:GetAAIOption("aai_panel")
    self.hudlist.bai.captain_panel = BAI:GetAAIOption("captain_panel")
end

function HUDAssaultCorner:InitHUDListOptions()
    local function Update()
        self:QueryBAIOptions()
        self:QueryHUDListOptions(true)
    end

    BAI:AddEvent(BAI.EventList.Update, Update)

    if self.hudlist.bai.assault_panel_position > 3 then
        return
    end

    local offset = 46
    self.hudlist.move_hudlist = self.hudlist.bai.assault_panel_position == 3
    if self.hudlist.hudlist_enabled and self.hudlist.hostages_hidden then
        BAI.settings.completely_hide_hostage_panel = true
        self:DisableHostagePanelFunctions()
        if self.hudlist.bai.assault_panel_position < 3 then
            offset = 0
        end
    end

    local delay = 0.75

    local function Endless()
        local offset = self._bg_box:h() + 8
        if self.hudlist.bai.assault_panel_position ~= 3 then
            if self.hudlist.hostages_hidden or BAI:IsHostagePanelHidden("endless") then
                offset = 0
            end
        else
            if not self.hudlist.hostages_hidden and BAI:IsHostagePanelVisible("endless") then
                offset = self._bg_box:h() + 54
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.EndlessAssaultStart, Endless, delay)

    local function NoReturn(active)
        local offset = self._bg_box:h() + 8
        if self.hudlist.bai.assault_panel_position < 3 then
            offset = 0
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.NoReturn, NoReturn, delay)

    local function NormalAssaultOverride()
        local offset = self._bg_box:h() + 54
        if self.hudlist.bai.assault_panel_position ~= 3 then
            if self.hudlist.hostages_hidden and self.hudlist.bai.aai_panel == AAIPanel.InTheAssaultBox then
                offset = 0
            else
                offset = 46
            end
        else
            if self.hudlist.hostages_hidden then
                if self.hudlist.bai.aai_panel == AAIPanel.InTheAssaultBox then
                    offset = 46
                end
            else
                if self.hudlist.bai.aai_panel == AAIPanel.InTheAssaultBox and BAI:IsHostagePanelHidden("assault") then
                    offset = 46
                end
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.NormalAssaultOverride, NormalAssaultOverride, delay)

    local function Assault()
        local offset = self._bg_box:h() + 54
        if self.hudlist.bai.assault_panel_position ~= 3 then
            if self.hudlist.hostages_hidden then
                offset = self.hudlist.bai.aai_panel == AAIPanel.InTheAssaultBox and 0 or 46
            else
                if self.hudlist.bai.aai_panel == AAIPanel.InTheAssaultBox then
                    offset = BAI:IsHostagePanelVisible("assault") and 46 or 0
                else
                    offset = 46
                end
            end
        else
            if self.hudlist.hostages_hidden and self.hudlist.bai.aai_panel == AAIPanel.InTheAssaultBox then
                offset = 46
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.AssaultStart, Assault, delay)

    local function AssaultEnd()
        local offset = self._bg_box:h() + 54
        if self.hudlist.bai.assault_panel_position ~= 3 then
            if self.hudlist.hostages_hidden then
                offset = 0
            else
                offset = 46
            end
        else
            if self.hudlist.hostages_hidden then
                if BAI:GetOption("show_assault_states") then
                    offset = BAI:IsStateEnabled("control") and (self._bg_box:h() + 8) or 46
                end
            else
                if BAI:GetOption("show_assault_states") and BAI:IsStateDisabled("control") then
                    offset = self._bg_box:h() + 8
                end
            end
        end
        self:MoveHUDList(offset)
        if self.hudlist.bai.assault_panel_position then
            managers.hud._hud_heist_timer._heist_timer_panel:set_visible(false)
        end
    end

    BAI:AddEvent(BAI.EventList.AssaultEnd, AssaultEnd, 0.90)

    local function Captain(active)
        local offset = self._bg_box:h() + 54
        if active then -- Captain panel is visible
            if self.hudlist.bai.assault_panel_position ~= 3 then
                if BAI:GetAAIOption("captain_panel") then
                    offset = 46
                else
                    offset = (self.hudlist.hostages_hidden or BAI:IsHostagePanelHidden("captain")) and 0 or 46
                end
            else
                offset = self._bg_box:h() + 54
                if not BAI:GetAAIOption("captain_panel") and (self.hudlist.hostages_hidden or BAI:IsHostagePanelHidden("captain")) then
                    offset = self._bg_box:h() + 8
                end
            end
        else
            local panel_visible = self.hudlist.bai.aai_panel == AAIPanel.CustomPanel
            local condition = BAI:IsHostagePanelVisible("assault") or panel_visible
            if self.hudlist.bai.assault_panel_position ~= 3 then
                if self.hudlist.hostages_hidden then
                    offset = panel_visible and 46 or 0
                else
                    offset = condition and 46 or 0
                end
            else
                if self.hudlist.hostages_hidden then
                    offset = self._bg_box:h() + (panel_visible and 54 or 8)
                else
                    offset = self._bg_box:h() + (condition and 54 or 8)
                end
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.Captain, Captain, delay)

    BAI:AddEvent("MoveHUDList", function(self)
        local offset = self._bg_box:h() + 54
        if self.hudlist.bai.assault_panel_position == 3 then
            if self.hudlist.hostages_hidden then
                offset = self._bg_box:h() + 8
            end
        else
            if self.hudlist.hostages_hidden then
                offset = 46
            end
        end
        if self.hudlist.bai.assault_panel_position == 1 then
            offset = 0
            self:ApplyObjectivesOffset(self._bg_box:bottom() + 12)
        end
        self:MoveHUDList(offset)
    end)

    self:MoveHUDList(offset)
end

local _BAI_InitAAIPanel = HUDAssaultCorner.InitAAIPanel
function HUDAssaultCorner:InitAAIPanel()
    _BAI_InitAAIPanel(self)
    if not (self.AAIPanel and self.hudlist.hostages_hidden) then
        return
    end
    if self:should_display_waves() then
        return
    end

    self._hud_panel:child("time_panel"):set_x(self._hud_panel:w() - self._hud_panel:child("time_panel"):w())
    self._hud_panel:child("spawns_panel"):set_right(self._hud_panel:child("time_panel"):left() - 3)
end

function HUDAssaultCorner:MoveHUDList(offset)
    if self.hudlist.hudlist_enabled and HUDListManager.change_setting then
        HUDListManager.change_setting("right_list_y", offset)
    end
end

function HUDAssaultCorner:ApplyObjectivesOffset(offset)
    if managers.hud._hud_objectives and managers.hud._hud_objectives.apply_offset then
        managers.hud._hud_objectives:apply_offset(offset)
    end
end

local _BAI_offset_hostages = HUDAssaultCorner._offset_hostages
function HUDAssaultCorner:_offset_hostages(hostage_panel, is_offseted, box_h)
    if self.hudlist.bai.assault_panel_position == 3 then
        _BAI_offset_hostages(self, hostage_panel, is_offseted, box_h)
    end
end

local _f_set_hostage_offseted = HUDAssaultCorner._set_hostage_offseted
function HUDAssaultCorner:_set_hostage_offseted(is_offseted)
    if self.hudlist.bai.assault_panel_position <= 3 then
        if is_offseted then
            self:start_assault_callback()
        elseif self.hudlist.move_hudlist then
            local offset = self.hudlist.hostages_hidden and 0 or 46
            self:MoveHUDList(offset)
        end
    else
        _f_set_hostage_offseted(self, is_offseted)
    end
end