local POS =
{
    LEFT = 1,
    CENTER = 2,
    RIGHT = 3
}
local AAIPanel =
{
    InTheAssaultBox = 1,
    CustomPanel = 2
}
local BAI = BAI
local BAIFunctionForced = false
function HUDAssaultCorner:ApplyCompatibility()
    if not WolfHUD then
        BAI:CrashWithErrorHUD("WolfHUD")
    end
    self.wolfhud = { bai = {} }
    self:QueryWolfHUDOptions()
    self:QueryBAIOptions()
    self:InitWolfHUDOptions()
end

local _w_change_assaultbanner_setting = HUDManager.change_assaultbanner_setting
function HUDManager:change_assaultbanner_setting(setting, value)
    _w_change_assaultbanner_setting(self, setting, value)
    if self._hud_assault_corner then
        self._hud_assault_corner:QueryWolfHUDOptions()
    end
end

function HUDAssaultCorner:QueryWolfHUDOptions(update)
    self.wolfhud.position = WolfHUD:getSetting({"AssaultBanner", "POSITION"}, 3)
    self.wolfhud.hudlist_show_hostages = WolfHUD:getSetting({"HUDList", "RIGHT_LIST", "show_hostages"}, true)
    if not update then
        self.wolfhud.hudlist_enabled = WolfHUD:getSetting({"HUDList", "ENABLED"}, true)
    end
end

function HUDAssaultCorner:QueryBAIOptions()
    self.wolfhud.bai.aai_visible = BAI:GetOption("show_advanced_assault_info")
    self.wolfhud.bai.aai_panel = BAI:GetAAIOption("aai_panel")
    self.wolfhud.bai.captain_panel = BAI:GetAAIOption("captain_panel")
end

function HUDAssaultCorner:InitWolfHUDOptions()
    local function Update()
        self:QueryBAIOptions()
        self:QueryWolfHUDOptions(true)
    end

    BAI:AddEvent(BAI.EventList.Update, Update)

    if not BAI:GetHUDOption("wolfhud", "move_hudlist") then
        return
    end

    local offset = 46
    self.wolfhud.move_hudlist = true
    if self.wolfhud.hudlist_enabled and self.wolfhud.hudlist_show_hostages then
        self.wolfhud.hostages_hidden = true
        if not BAI.settings.completely_hide_hostage_panel then
            BAI.settings.completely_hide_hostage_panel = true
            self:DisableHostagePanelFunctions()
            BAIFunctionForced = true
        end
        self._hud_panel:child("hostages_panel"):set_visible(false)
        self._hud_panel:child("hostages_panel"):set_alpha(0)
        offset = 0
    end

    local delay = 0.75

    local function Endless()
        local offset = self._bg_box:h() + 8
        if self.wolfhud.position ~= POS.RIGHT and self.wolfhud.hostages_hidden then
            offset = 0
        else
            if not self.wolfhud.hostages_hidden and BAI:IsHostagePanelVisible("endless") then
                offset = self._bg_box:h() + 54
            end
        end
        if self.wolfhud.position == POS.LEFT then
            self:ApplyObjectivesOffset(self._bg_box:bottom() + 12)
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.EndlessAssaultStart, Endless, delay)

    local function NoReturn(active)
        local offset = self._bg_box:h() + 8
        if self.wolfhud.position ~= POS.RIGHT then
            offset = 0
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.NoReturn, NoReturn, delay)

    local function NormalAssaultOverride()
        local offset = self._bg_box:h() + 54
        if self.wolfhud.position ~= POS.RIGHT then
            if self.wolfhud.hostages_hidden and self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox then
                offset = 0
            else
                offset = 46
            end
        else
            if self.wolfhud.hostages_hidden then
                if self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox then
                    offset = 46
                end
            else
                if self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox and BAI:IsHostagePanelHidden("assault") then
                    offset = 46
                end
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.NormalAssaultOverride, NormalAssaultOverride, delay)

    local function Assault()
        local offset = self._bg_box:h() + 54
        if self.wolfhud.position ~= POS.RIGHT then
            if self.wolfhud.position == POS.LEFT then
                self:ApplyObjectivesOffset(self._bg_box:bottom() + 12)
            end
            if self.wolfhud.hostages_hidden then
                offset = self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox and 0 or 46
            else
                if self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox then
                    offset = BAI:IsHostagePanelVisible("assault") and 46 or 0
                else
                    offset = 46
                end
            end
        else
            if self.wolfhud.hostages_hidden and self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox then
                offset = 46
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.AssaultStart, Assault, delay)

    local function AssaultEnd()
        local offset = self._bg_box:h() + 54
        if self.wolfhud.position ~= POS.RIGHT then
            if self.wolfhud.hostages_hidden then
                offset = 0
            else
                offset = BAI:GetOption("completely_hide_hostage_panel") and 0 or 46
            end
        else
            if self.wolfhud.hostages_hidden then
                if BAI:GetOption("show_assault_states") then
                    offset = BAI:IsStateEnabled("control") and 46 or 0
                end
            else
                if BAI:GetOption("show_assault_states") and BAI:IsStateDisabled("control") then
                    offset = 46
                end
            end
            --[[if BAI:GetOption("completely_hide_hostage_panel") then
                if BAI:GetOption("show_wave_survived") then
                end
                if BAI:GetOption("show_assault_states") then
                    if BAI:IsStateEnabled("control") then
                        offset = 46
                    end
                else
                end
            end
            if BAI:GetOption("show_assault_states") then
                offset = 46
            else
                offset = BAI:GetOption("completely_hide_hostage_panel") and 0 or 46
            end]]
        end
        if self.wolfhud.position == POS.LEFT then
            local o_offset = 0
            local show_assault_states = BAI:GetOption("show_assault_states")
            if BAI:GetOption("show_wave_survived") then
                if not show_assault_states or (show_assault_states and BAI:IsStateDisabled("control")) then
                    local function MoveObjectives()
                        self:ApplyObjectivesOffset(0)
                    end
                    BAI:DelayCall("WolfHUD_MoveObjectivs", 8.6, MoveObjectives)
                end
                o_offset = self._bg_box:bottom() + 12
            else
                if show_assault_states and BAI:IsStateEnabled("control") then
                    o_offset = self._bg_box:bottom() + 12
                end
            end
            self:ApplyObjectivesOffset(o_offset)
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.AssaultEnd, AssaultEnd, delay + 0.15)

    local function Captain(active)
        local offset = self._bg_box:h() + 54
        if active then
            if self.wolfhud.position == POS.RIGHT then
                if self.wolfhud.hostages_hidden then
                    if not self.wolfhud.bai.captain_panel then
                        offset = self._bg_box:h() + 8
                    end
                else
                    if not self.wolfhud.bai.captain_panel or BAI:IsHostagePanelHidden("captain") then
                        offset = self._bg_box:h() + 8
                    end
                end
            else
                if self.wolfhud.hostages_hidden then
                    offset = self.wolfhud.bai.captain_panel and 46 or 0
                else
                    offset = (self.wolfhud.bai.captain_panel or BAI:IsHostagePanelVisible("captain")) and 46 or 0
                end
            end
        else
            if self.wolfhud.position == POS.RIGHT then
                if self.wolfhud.hostages_hidden then
                    if self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox then
                        offset = self._bg_box:h() + 8
                    end
                else
                    if self.wolfhud.bai.aai_panel == AAIPanel.InTheAssaultBox and BAI:IsHostagePanelHidden("assault") then
                        offset = self._bg_box:h() + 8
                    end
                end
            else
                if self.wolfhud.hostages_hidden then
                    offset = (self.wolfhud.bai.aai_panel == AAIPanel.CustomPanel) and 46 or 0
                else
                    offset = (self.wolfhud.bai.aai_panel == AAIPanel.CustomPanel or BAI:IsHostagePanelVisible("assault")) and 46 or 0
                end
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.Captain, Captain, delay)

    BAI:AddEvent("MoveHUDList", function(self)
        local offset = self._bg_box:h() + 54
        if self.wolfhud.position == POS.RIGHT then
            if self.wolfhud.hostages_hidden then
                offset = self._bg_box:h() + 8
            end
        else
            if not self.wolfhud.hostages_hidden or BAI:IsHostagePanelVisible() then
                offset = 46
            end
        end
        if self.wolfhud.position == POS.LEFT then
            offset = 0
            self:ApplyObjectivesOffset(self._bg_box:bottom() + 12)
        end
        self:MoveHUDList(offset)
    end)

    self:MoveHUDList(offset)

    if self.wolfhud.hudlist_enabled then
        self.update_hudlist_offset = function() end
        self.update_banner_pos = function() end
        if self.wolfhud.position == POS.LEFT then
            local buffs_panel = self._hud_panel:child("buffs_panel")
            local icon_assaultbox = self._hud_panel:child("assault_panel"):child("icon_assaultbox")
            buffs_panel:set_right(icon_assaultbox:left() + icon_assaultbox:w())
        end
    end
end

local _BAI_InitAAIPanel = HUDAssaultCorner.InitAAIPanel
function HUDAssaultCorner:InitAAIPanel()
    _BAI_InitAAIPanel(self)
    local condition = false -- Variable to determine if we should move the AAI panel
    if not (self.AAIPanel and self.wolfhud.hudlist_show_hostages) then
        condition = true
    end
    if self:should_display_waves() then
        if condition then -- Looks like Vanilla hostage panel has been disabled by WolfHUD's HUDList, let's move the AAI Panel in Safehouse Raid/Holdout
            condition = false
        else -- Don't move the AAI Panel => it would clip with the Wave Panel
            condition = true
        end
    end
    if condition then
        return
    end

    self._hud_panel:child("time_panel"):set_x(self._hud_panel:w() - self._hud_panel:child("time_panel"):w())
    self._hud_panel:child("spawns_panel"):set_right(self._hud_panel:child("time_panel"):left() - 3)
end

function HUDAssaultCorner:MoveHUDList(offset)
    if self.wolfhud.hudlist_enabled and managers.hud.change_list_setting then
        managers.hud:change_list_setting("right_list_height_offset", offset)
    end
end

function HUDAssaultCorner:ApplyObjectivesOffset(offset)
    if managers.hud._hud_objectives and managers.hud._hud_objectives.apply_offset then
        managers.hud._hud_objectives:apply_offset(offset)
    end
end

local _BAI_offset_hostages = HUDAssaultCorner._offset_hostages
function HUDAssaultCorner:_offset_hostages(hostage_panel, is_offseted, box_h)
    if self.wolfhud.position == POS.RIGHT then
        _BAI_offset_hostages(self, hostage_panel, is_offseted, box_h)
    end
end

local _f_set_hostage_offseted = HUDAssaultCorner._set_hostage_offseted
function HUDAssaultCorner:_set_hostage_offseted(is_offseted)
    if self.wolfhud.position < POS.RIGHT then -- Left and Center
        if is_offseted then
            self:start_assault_callback()
        elseif self.wolfhud.move_hudlist then
            local offset = self.wolfhud.hostages_hidden and 0 or (BAI:GetOption("completely_hide_hostage_panel") and 0 or 46)
            self:MoveHUDList(offset)
        end
    else
        _f_set_hostage_offseted(self, is_offseted)
    end
end