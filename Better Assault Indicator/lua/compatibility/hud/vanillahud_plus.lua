local AAIPanel =
{
    InTheAssaultBox = 1,
    CustomPanel = 2
}
local BAI = BAI
local BAIFunctionForced = false
function HUDAssaultCorner:ApplyCompatibility()
    if not VHUDPlus then
        BAI:CrashWithErrorHUD("VanillaHUD Plus")
    end
    self.vhudplus = { bai = {} }
    self:QueryVHUDPlusOptions()
    self:QueryBAIOptions()
    self:InitVHUDOptions()
    BAI:AddEvent("MoveHUDList", function(self)
        if self.vhudplus.center and not VHUDPlus:getSetting({"AssaultBanner", "MUI_ASSAULT_FIX"}, false) then
            managers.hud._hud_heist_timer._heist_timer_panel:set_visible(false)
        end
    end)
end

function HUDAssaultCorner:QueryVHUDPlusOptions(update)
    self.vhudplus.center = VHUDPlus:getSetting({"AssaultBanner", "USE_CENTER_ASSAULT"}, true)
    self.vhudplus.hostages_hidden = not VHUDPlus:getSetting({"HUDList", "ORIGNIAL_HOSTAGE_BOX"}, false)
    if not update then
        self.vhudplus.hudlist_enabled = VHUDPlus:getSetting({"HUDList", "ENABLED"}, true)
    end
end

function HUDAssaultCorner:QueryBAIOptions()
    self.vhudplus.bai.aai_visible = BAI:GetOption("show_advanced_assault_info")
    self.vhudplus.bai.aai_panel = BAI:GetAAIOption("aai_panel")
    self.vhudplus.bai.captain_panel = BAI:GetAAIOption("captain_panel")
end

function HUDAssaultCorner:InitVHUDOptions()
    local function Update()
        self:QueryBAIOptions()
        self:QueryVHUDPlusOptions(true)
    end

    BAI:AddEvent(BAI.EventList.Update, Update)

    if not BAI:GetHUDOption("vanillahud_plus", "move_hudlist") then
        local function AssaultEnd()
            if self.vhudplus.center and BAI:GetOption("show_assault_states") and BAI:IsStateEnabled("control") then
                managers.hud._hud_heist_timer._heist_timer_panel:set_visible(false)
            end
        end

        BAI:AddEvent(BAI.EventList.AssaultEnd, AssaultEnd, 0.90)
        return
    end

    local offset = 46
    self.vhudplus.move_hudlist = true
    if self.vhudplus.hudlist_enabled then
        if self.vhudplus.hostages_hidden or BAI:GetOption("completely_hide_hostage_panel") then
            BAI.settings.completely_hide_hostage_panel = true
            self:DisableHostagePanelFunctions()
            if self.vhudplus.center then
                offset = 0
            end
        end
    end

    local delay = 0.75

    local function Endless()
        local offset = self._bg_box:h() + 8
        if self.vhudplus.center then
            if self.vhudplus.hostages_hidden or BAI:IsHostagePanelHidden("endless") then
                offset = 0
            end
        else
            if not self.vhudplus.hostages_hidden and BAI:IsHostagePanelVisible("endless") then
                offset = self._bg_box:h() + 54
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.EndlessAssaultStart, Endless, delay)

    local function NoReturn(active)
        local offset = self._bg_box:h() + 8
        if self.vhudplus.center then
            offset = 0
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.NoReturn, NoReturn, delay)

    local function NormalAssaultOverride()
        local offset = self._bg_box:h() + 54
        if self.vhudplus.center then
            if self.vhudplus.hostages_hidden and self.vhudplus.bai.aai_panel == AAIPanel.InTheAssaultBox then
                offset = 0
            else
                offset = 46
            end
        else
            if self.vhudplus.hostages_hidden then
                if self.vhudplus.bai.aai_panel == AAIPanel.InTheAssaultBox then
                    offset = 46
                end
            else
                if self.vhudplus.bai.aai_panel == AAIPanel.InTheAssaultBox and BAI:IsHostagePanelHidden("assault") then
                    offset = 46
                end
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.NormalAssaultOverride, NormalAssaultOverride, delay)

    local function Assault()
        local offset = self._bg_box:h() + 54
        if self.vhudplus.center then
            if self.vhudplus.hostages_hidden then
                offset = self.vhudplus.bai.aai_panel == AAIPanel.InTheAssaultBox and 0 or 46
            else
                if self.vhudplus.bai.aai_panel == AAIPanel.InTheAssaultBox then
                    offset = BAI:IsHostagePanelVisible("assault") and 46 or 0
                else
                    offset = 46
                end
            end
        else
            if self.vhudplus.hostages_hidden and self.vhudplus.bai.aai_panel == AAIPanel.InTheAssaultBox then
                offset = 46
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.AssaultStart, Assault, delay)

    local function AssaultEnd()
        local offset = self._bg_box:h() + 54
        if self.vhudplus.center then
            if self.vhudplus.hostages_hidden or BAI:GetOption("completely_hide_hostage_panel") then
                offset = 0
            else
                offset = 46
            end
        else
            if self.vhudplus.hostages_hidden or BAI:GetOption("completely_hide_hostage_panel")  then
                if BAI:GetOption("show_assault_states") then
                    offset = BAI:IsStateEnabled("control") and (self._bg_box:h() + 8) or 46
                end
            else
                if BAI:GetOption("show_assault_states") and BAI:IsStateDisabled("control") then
                    offset = self._bg_box:h() + 8
                end
            end
        end
        self:MoveHUDList(offset)
        if self.vhudplus.center then
            managers.hud._hud_heist_timer._heist_timer_panel:set_visible(false)
        end
    end

    BAI:AddEvent(BAI.EventList.AssaultEnd, AssaultEnd, 0.90)

    local function Captain(active)
        local offset = self._bg_box:h() + 54
        if active then -- Captain panel is visible
            if self.vhudplus.center then
                if BAI:GetAAIOption("captain_panel") then
                    offset = 46
                else
                    offset = (self.vhudplus.hostages_hidden or BAI:IsHostagePanelHidden("captain")) and 0 or 46
                end
            else
                offset = self._bg_box:h() + 54
                if not BAI:GetAAIOption("captain_panel") and (self.vhudplus.hostages_hidden or BAI:IsHostagePanelHidden("captain")) then
                    offset = self._bg_box:h() + 8
                end
            end
        else
            local panel_visible = self.vhudplus.bai.aai_panel == AAIPanel.CustomPanel
            local condition = BAI:IsHostagePanelVisible("assault") or panel_visible
            if self.vhudplus.center then
                if self.vhudplus.hostages_hidden then
                    offset = panel_visible and 46 or 0
                else
                    offset = condition and 46 or 0
                end
            else
                if self.vhudplus.hostages_hidden then
                    offset = self._bg_box:h() + (panel_visible and 54 or 8)
                else
                    offset = self._bg_box:h() + (condition and 54 or 8)
                end
            end
        end
        self:MoveHUDList(offset)
    end

    BAI:AddEvent(BAI.EventList.Captain, Captain, delay)

    self:MoveHUDList(offset)
end

local _BAI_InitAAIPanel = HUDAssaultCorner.InitAAIPanel
function HUDAssaultCorner:InitAAIPanel()
    _BAI_InitAAIPanel(self)
    if not (self.AAIPanel and self.vhudplus.hostages_hidden) then
        return
    end
    if self:should_display_waves() then
        return
    end

    self._hud_panel:child("time_panel"):set_x(self._hud_panel:w() - self._hud_panel:child("time_panel"):w())
    self._hud_panel:child("spawns_panel"):set_right(self._hud_panel:child("time_panel"):left() - 3)
end

function HUDAssaultCorner:MoveHUDList(offset)
    if self.vhudplus.hudlist_enabled and managers.hud.change_list_setting then
        managers.hud:change_list_setting("right_list_height_offset", offset)
    end
end

local _BAI_offset_hostages = HUDAssaultCorner._offset_hostages
function HUDAssaultCorner:_offset_hostages(hostage_panel, is_offseted, box_h)
    if not self.vhudplus.center then
        _BAI_offset_hostages(self, hostage_panel, is_offseted, box_h)
    end
end

local _f_set_hostage_offseted = HUDAssaultCorner._set_hostage_offseted
function HUDAssaultCorner:_set_hostage_offseted(is_offseted)
    if self.vhudplus.center then
        if is_offseted then
            self:start_assault_callback()
        elseif self.vhudplus.move_hudlist then
            local offset = self.vhudplus.hostages_hidden and 0 or (BAI:GetOption("completely_hide_hostage_panel") and 0 or 46)
            self:MoveHUDList(offset)
        end
    else
        _f_set_hostage_offseted(self, is_offseted)
    end
end