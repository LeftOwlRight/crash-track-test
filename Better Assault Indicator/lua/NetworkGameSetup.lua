local function WhichOverride(current_level_id)
    if current_level_id == "spa" then
        return "elementdifficulty.lua"
    elseif current_level_id == "born" or current_level_id == "dah" or current_level_id == "man" or current_level_id == "jolly" or current_level_id == "dinner" or current_level_id == "kenaz" then
        return "coreelementinstance.lua"
    elseif current_level_id == "pbr" then
        return "beneath_the_mountain.lua"
    elseif current_level_id == "glace" or current_level_id == "peta2" or current_level_id == "hox_2" or current_level_id == "ukrainian_job" then
        return "coreelementunitsequencetrigger.lua"
    elseif current_level_id == "pbr2" then
        return "birth_of_sky.lua"
    elseif current_level_id == "hox_1" then
        return "elementareatrigger.lua"
    elseif current_level_id == "mad" or current_level_id == "sah" then
        return "coreelementcounter.lua"
    elseif current_level_id == "help" then
        return "elementenemypreferedadd.lua"
    elseif current_level_id == "crojob2" then
        return "the_bomb_dockyard.lua"
    elseif current_level_id == "pines" then
        return "white_xmas.lua"
    elseif current_level_id == "office_strike" then -- custom heist
        return "office_strike.lua"
    else --"rvd2", "red2", "vit"
        return "missionscriptelement.lua"
    end
end

--[[
    Added load function to NetworkGameSetup
]]
function NetworkGameSetup:load(data)
    GameSetup.load(self, data)
    if Network:is_client() then
        LuaNetworking:SendToPeer(1, BAI.SyncMessage, BAI.data.BAI_Q)
        BAI:DelayCall("DelayHostDetection_BAI", 1.5, function()
            if not managers.hud:GetCompatibleHost() and BAI:IsPlayingSupportedHeistWithEA() then
                dofile(BAI.ClientPath .. WhichOverride(BAI._cache.level_id))
            end
        end)
    else
        BAI:Log("Host tried to execute client's code. Ignoring...")
    end
end