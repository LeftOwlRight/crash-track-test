--[[
    Code is not executed
    It may be used in the future
]]

local function IsLevelSupported(level_id)
    return BAI:IsOr(level_id, "man", "mad", "dinner")
end

local function GetNumberOfObjectivesToCheck(level_id)
    if level_id == "man" then
        return 1
    elseif level_id == "mad" then
        return 2
    elseif level_id == "dinner" then
        return 3
    else
        return 0
    end
end

local function GetLevelObjectivesToCheck(level_id)
    if level_id == "man" then
        return "heist_man11"
    elseif level_id == "mad" then
        return { "heist_mad_11", "heist_mad_12" }
    elseif level_id == "dinner" then
        return { "dinner_fight_escape", "dinner_ignite", "dinner_escape" }
    else
        return nil
    end
end

local function IsEndlessAssaultNonOverridable(level_id)
    return level_id ~= "dinner"
end

local function IsDelay(level_id)
    return level_id == "mad"
end

local function Delay(level_id)
    return level_id == "mad" and 2 or 0
end

local function TriggerEndlessAssault(dont_override)
    managers.hud:StartEndlessAssaultClient(dont_override)
end

local function IterateObjectives()
    local level_id = Global.game_settings.level_id
    local activated = false
    for _, v in pairs(GetLevelObjectivesToCheck(level_id)) do
        if managers.objectives._active_objectives[v] then
            activated = true
            TriggerEndlessAssault(IsEndlessAssaultNonOverridable(level_id))
            break
        end
    end
    if not activated then
        local function GetObjectivesInString(objectives)
            local n = #objectives
            local s = ""
            for i, v in ipairs(objectives) do
                s = s .. v
                if i + 1 == n then
                    s = s .. " and " .. objectives[i + 1]
                    break
                else
                    s = s .. ", "
                end
            end
            return s
        end
        log(string.format("[BAI - ObjectivesManager] Objectives %s are not active", GetObjectivesInString(GetLevelObjectivesToCheck(level_id))))
    end
end

local _f_load = ObjectivesManager.load
function ObjectivesManager:load(data)
    _f_load(self, data)
    local state = data.ObjectivesManager

    if state and not managers.hud:GetBAIHost() then
        local level_id = Global.game_settings.level_id
        if IsLevelSupported(level_id) then
            if GetNumberOfObjectivesToCheck(level_id) > 1 then
                if IsDelay(level_id) then
                    BAI:DelayCall("EndlessAssaultCheckDelay", Delay(level_id), IterateObjectives)
                else
                    IterateObjectives()
                end
            else
                if self._active_objectives[GetLevelObjectivesToCheck(level_id)] then
                    TriggerEndlessAssault(IsEndlessAssaultNonOverridable(level_id))
                else
                    log(string.format("[BAI - ObjectivesManager] Objective %s is not active", GetLevelObjectivesToCheck(level_id)))
                end
            end
        else
            log(string.format("[BAI - ObjectivesManager] Level %s is not supported! Aborting", level_id))
        end
    end
end